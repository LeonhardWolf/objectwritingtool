const expandContractWidget = (widgetName, widgetState, store) => {
  store.dispatch("setWidgetExpanded", {
    widget: widgetName,
    state: widgetState,
  });
};

const tryExpanding = (widgetId) => {
  const element = document.getElementById(widgetId);

  element.classList.remove("contract");
  element.classList.add("expand");
};
const tryContracting = (widgetId) => {
  const element = document.getElementById(widgetId);

  if (element.classList.contains("expand")) {
    element.classList.remove("expand");
    element.classList.add("contract");
  }
};

module.exports = { expandContractWidget, tryExpanding, tryContracting };
