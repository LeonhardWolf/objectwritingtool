const allEntries = [
  {
    _id: 1,
    title: "Lamppost",
    translation: "Laternenmast",
    date: new Date("2021-06-21"),
    fields: [
      {
        _id: 1,
        sense: "sight",
        bodyText:
          "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit dolor architecto commodi iusto ea porro quisquam nam delectus quam unde!",
      },
      {
        _id: 2,
        sense: "hearing",
        bodyText:
          "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit dolor architecto commodi iusto ea porro quisquam nam delectus quam unde!",
      },
      {
        _id: 3,
        sense: "smell",
        bodyText:
          "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit dolor architecto commodi iusto ea porro quisquam nam delectus quam unde!",
      },
      {
        _id: 4,
        sense: "taste",
        bodyText:
          "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit dolor architecto commodi iusto ea porro quisquam nam delectus quam unde!",
      },
    ],
  },
  {
    _id: 2,
    title: "Shark",
    translation: "Hai",
    date: new Date("2021-06-22"),
    fields: [
      {
        _id: 1,
        sense: "touch",
        bodyText: "Number One",
      },
      {
        _id: 2,
        sense: "organic",
        bodyText: "Number Two",
      },
    ],
  },
];

const GETResBody = [
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98300",
    title: "Bucket",
    translation: "Eimer",
    date: "2021-08-17T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f97310",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: "5099803df3f4948bd2f97311",
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98301",
    title: "Table",
    translation: "Tisch",
    date: "2021-08-18T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f98310",
        sense: "smell",
        bodyText: "I belong to table.",
      },
      {
        _id: "5099803df3f4948bd2f98311",
        sense: "taste",
        bodyText: "I belong to table, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98302",
    title: "Fence",
    translation: "Zaun",
    date: "2021-08-19T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f99310",
        sense: "touch",
        bodyText: "I belong to fence.",
      },
      {
        _id: "5099803df3f4948bd2f99311",
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
    ],
  },
];

const expectedGETEntries = [
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98300",
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: [
      {
        _id: "5099803df3f4948bd2f97310",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: "5099803df3f4948bd2f97311",
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98301",
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: [
      {
        _id: "5099803df3f4948bd2f98310",
        sense: "smell",
        bodyText: "I belong to table.",
      },
      {
        _id: "5099803df3f4948bd2f98311",
        sense: "taste",
        bodyText: "I belong to table, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98302",
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: [
      {
        _id: "5099803df3f4948bd2f99310",
        sense: "touch",
        bodyText: "I belong to fence.",
      },
      {
        _id: "5099803df3f4948bd2f99311",
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
    ],
  },
];

module.exports = {
  allEntries,
  expectedGETEntries,
  GETResBody,
};
