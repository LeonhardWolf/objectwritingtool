import Vuex from "vuex";
import Vue from "vue";
import all from "./modules/all";

// load Vuex
Vue.use(Vuex);

// create store
export default new Vuex.Store({
  modules: {
    all,
  },
});
