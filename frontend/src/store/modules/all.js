import axios from "axios";
import _ from "lodash";
import { getNextSense, getNextNonDbId, yearMonthDayEqual } from "./storeLogic";

const parseDateString = (stringEntries) => {
  const dateEntries = stringEntries.map((entry) => ({
    ...entry,
    date: new Date(entry.date),
  }));
  return dateEntries;
};

const state = {
  serverReqFailed: false,
  serverReqFinished: false,
  GETEntries: [],
  currentEntries: [],
  selectedEntry: [],
  PUTRequestScheduled: false,
  timerExpanded: false,
  calendarExpanded: false,
  searchBarExpanded: false,
};

const getters = {
  getServerReqFailed: (state) => {
    return state.serverReqFailed;
  },

  getServerReqFinished: (state) => {
    return state.serverReqFinished;
  },

  getCurrentEntries: (state) => {
    return state.currentEntries;
  },

  getSelectedEntryFields: (state) => {
    if (state.currentEntries === [])
      return { _id: null, sense: null, bodyText: null };

    return state.selectedEntry.fields;
  },

  getSelectedEntryTitle: (state) => {
    return state.selectedEntry.title;
  },

  getSelectedEntryTranslation: (state) => {
    return state.selectedEntry.translation;
  },

  getTimerExpanded: (state) => {
    return state.timerExpanded;
  },
  getCalendarExpanded: (state) => {
    // setTimeout(() => {
    //   console.log(`inside getter after 1 sec.: ${state.calendarExpanded}`);
    // }, 1000);
    return state.calendarExpanded;
  },
  getSearchBarExpanded: (state) => {
    return state.searchBarExpanded;
  },
};

const actions = {
  async fetchEntries({ commit }) {
    const res = await axios
      .get(
        `${window.location.protocol}//${window.location.hostname}:${process.env.VUE_APP_BACKEND_PORT}/api/entries`
      )
      .catch((err) => {
        console.error(err);
        commit("setServerReqFailed", true);
      });

    commit("setServerReqFinished", true);
    if (!res) return;

    const dateParsedResBody = parseDateString(res.data);

    commit("setGETEntries", dateParsedResBody);
    commit("setCurrentEntries", dateParsedResBody);
    const entryOfToday = dateParsedResBody[dateParsedResBody.length - 1];
    commit("setSelectedEntry", entryOfToday);
  },

  async schedulePUTRequest({ commit, state }) {
    if (state.PUTRequestScheduled === false) {
      commit("setPUTRequestScheduled", true);
      // state.PUTRequestScheduled = true;

      setTimeout(async () => {
        // console.log(
        // `currentEntries: ${state.currentEntries[3].fields[0].bodyText}`
        // );
        // console.log(`GETEntries: ${state.GETEntries[3].fields[0].bodyText}`);

        const checkFieldsBodyTextEqual = (currentEntry, GETEntry) => {
          let unequal = false;
          currentEntry.fields.forEach((field, index) => {
            if (GETEntry.fields.length <= index) return (unequal = true);
            if (field.bodyText !== GETEntry.fields[index].bodyText)
              unequal = true;
          });
          return unequal;
        };

        const changedEntries = state.currentEntries.filter((entry, index) =>
          checkFieldsBodyTextEqual(entry, state.GETEntries[index])
        );

        // console.log(`changedEntries: ${changedEntries}`);

        const PUTResponse = await axios.put(
          `${window.location.protocol}//${window.location.hostname}:${process.env.VUE_APP_BACKEND_PORT}/api/entries`,
          changedEntries
        );
        const dateParsedResBody = parseDateString(PUTResponse.data);
        commit("setGETEntries", dateParsedResBody);

        commit("setPUTRequestScheduled", false);
      }, 5000);
    }
  },

  updateSingleField({ commit, dispatch, state }, fieldToUpdate) {
    const entryIndex = state.currentEntries.findIndex(
      (entry) =>
        entry.fields.some((item) => item._id === fieldToUpdate._id) === true
    );

    // if (entryIndex === -1) return;

    const fieldIndex = state.currentEntries[entryIndex].fields.findIndex(
      (field) => field._id === fieldToUpdate._id
    );

    // if (fieldIndex === -1) return;

    commit("updateSingleField", [entryIndex, fieldIndex, fieldToUpdate]);

    const isLastFieldIndex = (entryIndex, fieldIndex) => {
      const lastFieldIndex = state.currentEntries[entryIndex].fields.length - 1;
      return fieldIndex === lastFieldIndex;
    };

    if (isLastFieldIndex(entryIndex, fieldIndex))
      commit("addSingleField", state.currentEntries[entryIndex]._id);

    const isSecondLastFieldEmptyUpdate = (entryIndex, fieldIndex, bodyText) => {
      const secondLastFieldIndex =
        state.currentEntries[entryIndex].fields.length - 2;

      if (fieldIndex !== secondLastFieldIndex) return false;
      if (bodyText !== "") return false;

      return true;
    };

    if (
      isSecondLastFieldEmptyUpdate(
        entryIndex,
        fieldIndex,
        fieldToUpdate.bodyText
      )
    )
      commit("deleteLastField", state.currentEntries[entryIndex]._id);

    dispatch("schedulePUTRequest");
  },

  addSingleField({ commit }, idOfCurrentEntry) {
    commit("addSingleField", idOfCurrentEntry);
  },

  deleteLastField({ commit }, idOfCurrentEntry) {
    commit("deleteLastField", idOfCurrentEntry);
  },

  setSelectedEntryByDate({ commit, state }, date) {
    const entryIndex = state.currentEntries.findIndex((entry) =>
      yearMonthDayEqual(entry.date, date)
    );
    if (entryIndex === -1) return console.error("Entry could not be found.");

    const entry = state.currentEntries[entryIndex];
    commit("setSelectedEntry", entry);
  },

  setSelectedEntryById({ commit, state }, id) {
    const entryIndex = state.currentEntries.findIndex(
      (entry) => entry._id === id
    );
    if (entryIndex === -1) return console.error("Entry could not be found.");

    const entry = state.currentEntries[entryIndex];
    commit("setSelectedEntry", entry);
  },

  setWidgetExpanded({ commit }, widgetAndState) {
    const widget = widgetAndState.widget;
    const state = widgetAndState.state;

    if (widget === "timer") {
      if (state) {
        commit("setTimerExpanded", true);
        commit("setCalendarExpanded", false);
        commit("setSearchBarExpanded", false);
      } else {
        commit("setTimerExpanded", false);
        commit("setCalendarExpanded", false);
        commit("setSearchBarExpanded", false);
      }
    } else if (widget === "calendar") {
      if (state) {
        commit("setTimerExpanded", false);
        commit("setCalendarExpanded", true);
        commit("setSearchBarExpanded", false);
      } else {
        commit("setTimerExpanded", false);
        commit("setCalendarExpanded", false);
        commit("setSearchBarExpanded", false);
      }
    } else if (widget === "searchBar") {
      if (state) {
        commit("setTimerExpanded", false);
        commit("setCalendarExpanded", false);
        commit("setSearchBarExpanded", true);
      } else {
        commit("setTimerExpanded", false);
        commit("setCalendarExpanded", false);
        commit("setSearchBarExpanded", false);
      }
    }
  },
};

const mutations = {
  setServerReqFailed: (state, boolean) => {
    state.serverReqFailed = boolean;
  },

  setServerReqFinished: (state, boolean) => {
    state.serverReqFinished = boolean;
  },

  setGETEntries: (state, GETEntries) =>
    (state.GETEntries = _.cloneDeep(GETEntries)),

  setCurrentEntries: (state, currentEntries) =>
    (state.currentEntries = _.cloneDeep(currentEntries)),

  setSelectedEntry: (state, entry) => {
    state.selectedEntry = entry;
  },

  setPUTRequestScheduled: (state, boolean) =>
    (state.PUTRequestScheduled = boolean),

  updateSingleField: (state, payload) => {
    const [entryIndex, fieldIndex, fieldToUpdate] = payload;

    state.currentEntries[entryIndex].fields[fieldIndex].bodyText =
      fieldToUpdate.bodyText;
  },

  addSingleField: (state, idOfCurrentEntry) => {
    const entryIndex = state.currentEntries.findIndex(
      (entry) => entry._id === idOfCurrentEntry
    );

    const lastCurrentId =
      state.currentEntries[entryIndex].fields[
        state.currentEntries[entryIndex].fields.length - 1
      ]._id;
    const nextId = getNextNonDbId(lastCurrentId);

    const lastCurrentSense =
      state.currentEntries[entryIndex].fields[
        state.currentEntries[entryIndex].fields.length - 1
      ].sense;
    const nextFieldSense = getNextSense(lastCurrentSense);

    state.currentEntries[entryIndex].fields.push({
      _id: `new[${nextId}]`,
      sense: nextFieldSense, // next sense in the list
      bodyText: "",
    });
  },

  deleteLastField: (state, idOfCurrentEntry) => {
    const entryIndex = state.currentEntries.findIndex(
      (entry) => entry._id === idOfCurrentEntry
    );

    state.currentEntries[entryIndex].fields.pop();
  },

  setTimerExpanded: (state, boolean) => {
    state.timerExpanded = boolean;
  },
  setCalendarExpanded: (state, boolean) => {
    state.calendarExpanded = boolean;
  },
  setSearchBarExpanded: (state, boolean) => {
    state.searchBarExpanded = boolean;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
