const senses = [
  "sight",
  "hearing",
  "smell",
  "taste",
  "touch",
  "organic",
  "kinesthetic",
];

const getNextSense = (lastSense) => {
  const lastIndex = senses.findIndex((sense) => sense === lastSense);
  if (lastIndex === -1) return console.error("next sense could not be found.");

  const sensesLength = senses.length;
  let nextIndex;

  if (lastIndex + 1 === sensesLength) {
    nextIndex = lastIndex + 1 - sensesLength;
  } else {
    nextIndex = lastIndex + 1;
  }

  const nextSense = senses[nextIndex];
  return nextSense;
};

const getLastNewId = (lastIdString) => {
  const leftCharacters = 4; // 'new[' is 4 characters
  const rightCharacters = lastIdString.search("]");
  const lastNewIndexString = lastIdString.slice(
    leftCharacters,
    rightCharacters
  );
  const lastNewIndex = parseInt(lastNewIndexString);
  return lastNewIndex;
};

const getNextNonDbId = (lastIdString) => {
  const firstFourCharacters = lastIdString.slice(0, 4);
  const lastCharacter = lastIdString.slice(-1);
  if (firstFourCharacters !== "new[" || lastCharacter !== "]") return 0;

  const lastNewId = getLastNewId(lastIdString);
  const nextNonDbId = lastNewId + 1;
  return nextNonDbId;
};

const yearMonthDayEqual = (date1, date2) => {
  const date1Clone = new Date(date1);
  const date2Clone = new Date(date2);

  const date1OffsetMins = date1Clone.getTimezoneOffset();
  const date1NoTimezoneOffset = new Date(
    date1Clone.setMinutes(date1Clone.getMinutes() + date1OffsetMins)
  );

  const date2OffsetMins = date2Clone.getTimezoneOffset();
  const date2NoTimezoneOffset = new Date(
    date2Clone.setMinutes(date2Clone.getMinutes() + date2OffsetMins)
  );

  const yearMonthDay1 = [
    date1NoTimezoneOffset.getFullYear(),
    date1NoTimezoneOffset.getMonth() + 1,
    date1NoTimezoneOffset.getDate(),
  ];
  const yearMonthDay2 = [
    date2NoTimezoneOffset.getFullYear(),
    date2NoTimezoneOffset.getMonth() + 1,
    date2NoTimezoneOffset.getDate(),
  ];

  return yearMonthDay1.every(
    (dateValue, index) => dateValue === yearMonthDay2[index]
  );
};

module.exports = {
  getNextSense,
  getLastNewId,
  getNextNonDbId,
  yearMonthDayEqual,
};
