import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { cloneDeep } from "lodash";
import _ from "lodash";

import Dates from "@/components/Dates.vue";
import storeConfig from "../../src/store/modules/all";
import { expectedGETEntries } from "../../src/mocks/Entries";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

const setupMockStore = () => {
  const actions = {
    setSelectedEntryByDate: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(Dates, {
    store,
    localVue,
  });
};

describe("Displays dates and titles of 'currentEntries' from store.", () => {
  const store = setupStore();
  store.state.currentEntries = _.cloneDeep(expectedGETEntries);

  const wrapper = createWrapper(store);

  it("Gets 'currentEntries' from store.", () => {
    expect(wrapper.vm.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("Updates 'currentEntries' when they are updated in the store.", () => {
    store.state.currentEntries = [
      {
        _id: 2,
        title: "Shark",
        translation: "Hai",
        date: new Date("2021-06-22"),
        fields: [
          {
            _id: 1,
            sense: "touch",
            bodyText: "Number One",
          },
          {
            _id: 2,
            sense: "organic",
            bodyText: "Number Two",
          },
        ],
      },
    ];
    expect(wrapper.vm.currentEntries).toStrictEqual([
      {
        _id: 2,
        title: "Shark",
        translation: "Hai",
        date: new Date("2021-06-22"),
        fields: [
          {
            _id: 1,
            sense: "touch",
            bodyText: "Number One",
          },
          {
            _id: 2,
            sense: "organic",
            bodyText: "Number Two",
          },
        ],
      },
    ]);
  });
});

describe("Dispatches 'setSelectedEntryByDate' when the date is changed.", () => {
  const [store, actions] = setupMockStore();
  const wrapper = createWrapper(store);

  it("Should set the data variable 'date' to '2021-08-20T00:00:00.000Z'.", () => {
    wrapper.setData({ date: new Date("2021-08-20T00:00:00.000Z") });
    expect(wrapper.vm.date).toStrictEqual(new Date("2021-08-20T00:00:00.000Z"));
  });

  it("Should emit '2021-08-19T00:00:00.000Z' from the input popover.", () => {
    const datePicker = wrapper.find("#datePicker");
    expect(datePicker.exists()).toBe(true);

    jest.resetAllMocks(); // I do not need the dispatched data from setting the initial date.
    datePicker.vm.$emit("input", "2021-08-19T00:00:00.000Z");
  });

  it("'setSelectedEntryByDate' with the chosen date '2021-08-19T00:00:00.000Z' should have been dispatched.", () => {
    expect(actions.setSelectedEntryByDate.mock.calls[0][1]).toBe(
      "2021-08-19T00:00:00.000Z"
    );
  });
});
