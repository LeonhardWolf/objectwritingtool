import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { cloneDeep } from "lodash";
import _ from "lodash";

import CurrentDayEntries from "@/components/CurrentDayEntries.vue";
import storeConfig from "../../src/store/modules/all";
import { expectedGETEntries } from "../../src/mocks/Entries";
import Vue from "vue";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

const setupMockStore = () => {
  const actions = {
    updateSingleField: jest.fn(),
    addSingleField: jest.fn(),
    deleteLastField: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(CurrentDayEntries, {
    store,
    localVue,
  });
};

describe("Fields of the selected entry should be rendered.", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Store set 'currentEntries' and 'selectedEntry'.", () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
  });

  it("'sense' and 'bodyText' of the current entry should be in the component HTML.", async () => {
    const senseTexts = await wrapper.findAll("#senseText");
    expect(senseTexts.exists()).toBe(true);
    expect(senseTexts.length).toBe(2);
    expect(senseTexts.at(0).text()).toBe("smell");
    expect(senseTexts.at(1).text()).toBe("taste");

    const textInputs = await wrapper.findAll("#textInput");
    expect(textInputs.exists()).toBe(true);
    expect(textInputs.length).toBe(2);
    expect(textInputs.at(0).element.value).toBe("I belong to table.");
    expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
  });
});

describe("Changing 'selectedEntry' should change the rendered fields accordingly.", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Should set the state 'currentEntries' and 'selectedEntry'.", () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
  });

  it("'sense' and 'bodyText' of the current entry should be in the component HTML.", async () => {
    const senseTexts = await wrapper.findAll("#senseText");
    expect(senseTexts.exists()).toBe(true);
    expect(senseTexts.length).toBe(2);
    expect(senseTexts.at(0).text()).toBe("smell");
    expect(senseTexts.at(1).text()).toBe("taste");

    const textInputs = await wrapper.findAll("#textInput");
    expect(textInputs.exists()).toBe(true);
    expect(textInputs.length).toBe(2);
    expect(textInputs.at(0).element.value).toBe("I belong to table.");
    expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
  });

  it("Store: change 'selectedEntry'.", () => {
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[2]);

    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[2]);
  });

  it("'sense' and 'bodyText' of the current entry should be updated in the component HTML.", async () => {
    const senseTexts = await wrapper.findAll("#senseText");
    expect(senseTexts.exists()).toBe(true);
    expect(senseTexts.length).toBe(2);
    expect(senseTexts.at(0).text()).toBe("touch");
    expect(senseTexts.at(1).text()).toBe("organic");

    const textInputs = await wrapper.findAll("#textInput");
    expect(textInputs.exists()).toBe(true);
    expect(textInputs.length).toBe(2);
    expect(textInputs.at(0).element.value).toBe("I belong to fence.");
    expect(textInputs.at(1).element.value).toBe("I belong to fence, too.");
  });
});

describe("Adding or deleting fields from the state 'currentEntries' should add or delete textInput fields.", () => {
  describe("Adding a field should create a new empty textInput at the bottom.", () => {
    const store = setupStore();
    const wrapper = createWrapper(store);

    it("Store set 'currentEntries' and 'selectedEntry'.", () => {
      store.state.currentEntries = _.cloneDeep(expectedGETEntries);
      store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

      expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
      expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
    });

    it("'sense' and 'bodyText' of the current entry should be in the component HTML.", async () => {
      const senseTexts = await wrapper.findAll("#senseText");
      expect(senseTexts.exists()).toBe(true);
      expect(senseTexts.length).toBe(2);
      expect(senseTexts.at(0).text()).toBe("smell");
      expect(senseTexts.at(1).text()).toBe("taste");

      const textInputs = await wrapper.findAll("#textInput");
      expect(textInputs.exists()).toBe(true);
      expect(textInputs.length).toBe(2);
      expect(textInputs.at(0).element.value).toBe("I belong to table.");
      expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
    });

    it("Committing 'addSingleField' should lead to a new empty textInput.", async () => {
      store.state.selectedEntry.fields = [
        {
          _id: "5099803df3f4948bd2f98310",
          sense: "smell",
          bodyText: "I belong to table.",
        },
        {
          _id: "5099803df3f4948bd2f98311",
          sense: "taste",
          bodyText: "I belong to table, too.",
        },
        {
          _id: "new[0]",
          sense: "touch",
          bodyText: "",
        },
      ];

      await Vue.nextTick();

      const senseTexts = await wrapper.findAll("#senseText");
      expect(senseTexts.exists()).toBe(true);
      expect(senseTexts.length).toBe(3);
      expect(senseTexts.at(0).text()).toBe("smell");
      expect(senseTexts.at(1).text()).toBe("taste");
      expect(senseTexts.at(2).text()).toBe("touch");

      const textInputs = await wrapper.findAll("#textInput");
      expect(textInputs.exists()).toBe(true);
      expect(textInputs.length).toBe(3);
      expect(textInputs.at(0).element.value).toBe("I belong to table.");
      expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
      expect(textInputs.at(2).element.value).toBe("");
    });
  });

  describe("Deleting a field should remove the last textInput.", () => {
    const store = setupStore();
    const wrapper = createWrapper(store);

    it("Store set 'currentEntries' and 'selectedEntry'.", async () => {
      store.state.currentEntries = _.cloneDeep(expectedGETEntries);
      store.state.selectedEntryId = "5099803df3f4948bd2f98301";
      await store.dispatch("setSelectedEntryById", "5099803df3f4948bd2f98301");

      expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
      expect(store.state.selectedEntryId).toBe("5099803df3f4948bd2f98301");
    });

    it("'sense' and 'bodyText' of the current entry should be in the component HTML.", async () => {
      const senseTexts = await wrapper.findAll("#senseText");
      expect(senseTexts.exists()).toBe(true);
      expect(senseTexts.length).toBe(2);
      expect(senseTexts.at(0).text()).toBe("smell");
      expect(senseTexts.at(1).text()).toBe("taste");

      const textInputs = await wrapper.findAll("#textInput");
      expect(textInputs.exists()).toBe(true);
      expect(textInputs.length).toBe(2);
      expect(textInputs.at(0).element.value).toBe("I belong to table.");
      expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
    });

    it("Committing 'addSingleField' should lead to a new empty textInput.", async () => {
      await store.commit("addSingleField", "5099803df3f4948bd2f98301");

      const senseTexts = await wrapper.findAll("#senseText");
      expect(senseTexts.exists()).toBe(true);
      expect(senseTexts.length).toBe(3);
      expect(senseTexts.at(0).text()).toBe("smell");
      expect(senseTexts.at(1).text()).toBe("taste");
      expect(senseTexts.at(2).text()).toBe("touch");

      const textInputs = await wrapper.findAll("#textInput");
      expect(textInputs.exists()).toBe(true);
      expect(textInputs.length).toBe(3);
      expect(textInputs.at(0).element.value).toBe("I belong to table.");
      expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
      expect(textInputs.at(2).element.value).toBe("");
    });

    it("Committing 'deleteLastField' should lead to only 2 textInputs.", async () => {
      await store.commit("deleteLastField", "5099803df3f4948bd2f98301");

      const senseTexts = await wrapper.findAll("#senseText");
      expect(senseTexts.exists()).toBe(true);
      expect(senseTexts.length).toBe(2);
      expect(senseTexts.at(0).text()).toBe("smell");
      expect(senseTexts.at(1).text()).toBe("taste");

      const textInputs = await wrapper.findAll("#textInput");
      expect(textInputs.exists()).toBe(true);
      expect(textInputs.length).toBe(2);
      expect(textInputs.at(0).element.value).toBe("I belong to table.");
      expect(textInputs.at(1).element.value).toBe("I belong to table, too.");
    });
  });
});
