import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { cloneDeep } from "lodash";
import _ from "lodash";

import SearchBar from "@/components/SearchBar.vue";
import storeConfig from "../../src/store/modules/all";
import { expectedGETEntries, GETResBody } from "../../src/mocks/Entries";
import all from "../../src/store/modules/all";
import Dropdown from "vue-simple-search-dropdown";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

const setupMockStore = () => {
  const actions = {
    updateSingleField: jest.fn(),
    addSingleField: jest.fn(),
    deleteLastField: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(SearchBar, {
    store,
    localVue,
  });
};

describe("Pull 'searchItems' from 'currentEntries':", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Should fill 'currentEntries'.", () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("Should only grab 'title' and '_id' from 'currentEntries'.", () => {
    const expectedSearchItems = [
      {
        _id: "5099803df3f4948bd2f98300",
        title: "Bucket",
      },
      {
        _id: "5099803df3f4948bd2f98301",
        title: "Table",
      },
      {
        _id: "5099803df3f4948bd2f98302",
        title: "Fence",
      },
    ];

    expect(wrapper.vm.searchItems.sort()).toStrictEqual(
      expectedSearchItems.sort()
    );
  });
});

describe("Typing in input filters results.", () => {
  describe("Data setup:", () => {
    const store = setupStore();
    const wrapper = createWrapper(store);

    it("No results without typing in the input field.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([]);
    });

    it("Should fill the state 'currentEntries'.", () => {
      store.state.currentEntries = _.cloneDeep(expectedGETEntries);
      expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    });

    it("Should hold the 'title' and 'id' of all entries in 'searchItems'.", () => {
      const expectedSearchItems = [
        {
          title: "Bucket",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Table",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Fence",
          _id: "5099803df3f4948bd2f98302",
        },
      ];

      expect(wrapper.vm.searchItems).toStrictEqual(expectedSearchItems);
    });
  });

  describe("Typing in the input field should filter out the results.", () => {
    const store = setupStore();
    const wrapper = createWrapper(store);

    it("Should set 'searchItems'.", () => {
      const dummyEntries = [
        {
          title: "Truck",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Trip",
          _id: "5099803df3f4948bd2f98302",
        },
      ];

      store.state.currentEntries = dummyEntries;

      expect(wrapper.vm.searchItems).toStrictEqual(dummyEntries);
    });
    it("Input with id 'searchInput' should exist.", () => {
      const searchInput = wrapper.find("#searchInput");
      expect(searchInput.exists()).toBe(true);
    });

    it("Should enter '' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("");

      expect(searchInput.element.value).toBe("");
    });
    it("Should have updated 'searchResults'.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([]);
    });

    it("Should enter 't' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("t");

      expect(searchInput.element.value).toBe("t");
    });
    it("Should have updated 'searchResults'.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([
        {
          title: "Truck",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Trip",
          _id: "5099803df3f4948bd2f98302",
        },
      ]);
    });

    it("Should enter 'tr' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("tr");

      expect(searchInput.element.value).toBe("tr");
    });
    it("Should have updated 'searchResults'.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([
        {
          title: "Truck",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Trip",
          _id: "5099803df3f4948bd2f98302",
        },
      ]);
    });

    it("Should enter 'tra' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("tra");

      expect(searchInput.element.value).toBe("tra");
    });
    it("Should have updated 'searchResults'.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
      ]);
    });

    it("Should enter 'trains' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("trains");

      expect(searchInput.element.value).toBe("trains");
    });
    it("'searchResults' should be empty.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([]);
    });

    // searchInput of of characters within a searchItem.title should not produce any results
    it("Should enter 'rain' into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("rain");

      expect(searchInput.element.value).toBe("rain");
    });
    it("'searchResults' should be empty.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([]);
    });
  });

  describe("Emptying the searchInput should set searchResults to '[]'.", () => {
    const store = setupStore();
    const wrapper = createWrapper(store);

    it("Should set 'searchItems'.", () => {
      const dummyEntries = [
        {
          title: "Truck",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Trip",
          _id: "5099803df3f4948bd2f98302",
        },
      ];

      store.state.currentEntries = dummyEntries;

      expect(wrapper.vm.searchItems).toStrictEqual(dummyEntries);
    });
    it("Input with id 'searchInput' should exist.", () => {
      const searchInput = wrapper.find("#searchInput");
      expect(searchInput.exists()).toBe(true);
    });
    it("Should set 'searchResults'.", () => {
      wrapper.vm.searchResults = [
        {
          title: "Truck",
          _id: "5099803df3f4948bd2f98300",
        },
        {
          title: "Train",
          _id: "5099803df3f4948bd2f98301",
        },
        {
          title: "Trip",
          _id: "5099803df3f4948bd2f98302",
        },
      ];
    });

    it("Should enter '' again into the searchInput.", () => {
      const searchInput = wrapper.find("#searchInput");
      searchInput.setValue("");

      expect(searchInput.element.value).toBe("");
    });
    it("Should have updated 'searchResults'.", () => {
      expect(wrapper.vm.searchResults).toStrictEqual([]);
    });
  });
});

describe("Rendering of results dropdown.", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Should set 'searchItems'.", () => {
    const dummyEntries = [
      {
        title: "Boat",
        _id: "5099803df3f4948bd2f98300",
      },
      {
        title: "Bliss",
        _id: "5099803df3f4948bd2f98301",
      },
      {
        title: "Bark",
        _id: "5099803df3f4948bd2f98302",
      },
    ];

    store.state.currentEntries = dummyEntries;

    expect(wrapper.vm.searchItems).toStrictEqual(dummyEntries);
  });

  it("'#searchResultsDropdown' should not be rendered when 'searchResults' is empty.", () => {
    const searchResultsDropdown = wrapper.find("#searchResultsDropdown");
    expect(searchResultsDropdown.exists()).toBe(false);
  });

  it("'searchInput' should exist.", () => {
    const searchInput = wrapper.find("#searchInput");
    expect(searchInput.exists()).toBe(true);
  });

  it("Should enter 'b' into the searchInput.", () => {
    const searchInput = wrapper.find("#searchInput");
    searchInput.setValue("b");

    expect(searchInput.element.value).toBe("b");
  });

  it("Should have all 3 elements in 'searchResults'.", () => {
    expect(wrapper.vm.searchResults).toStrictEqual([
      {
        title: "Boat",
        _id: "5099803df3f4948bd2f98300",
      },
      {
        title: "Bliss",
        _id: "5099803df3f4948bd2f98301",
      },
      {
        title: "Bark",
        _id: "5099803df3f4948bd2f98302",
      },
    ]);
  });

  it("Should render all 3 '.searchResult' in '#searchResultsDropdown'.", () => {
    const searchResultsDropdown = wrapper.find("#searchResultsDropdown");
    expect(searchResultsDropdown.exists()).toBe(true);

    const searchResults = wrapper.findAll(".searchResult");
    expect(searchResults.exists()).toBe(true);
    expect(searchResults.length).toBe(3);

    expect(searchResults.at(0).text()).toBe("Boat");
    expect(searchResults.at(1).text()).toBe("Bliss");
    expect(searchResults.at(2).text()).toBe("Bark");
  });
});

describe("Clicking on result changes selectedEntry:", () => {
  const setupMockStore = () => {
    const state = _.cloneDeep(all.state);
    const getters = _.cloneDeep(all.getters);
    const actions = {
      setSelectedEntryById: jest.fn(),
    };
    const mutations = {
      someFunction: jest.fn(),
    };

    const store = new Vuex.Store({
      state,
      getters,
      actions,
      mutations,
    });
    return [store, actions];
  };
  const [store, actions] = setupMockStore();
  const wrapper = createWrapper(store);

  it("Should set 'searchItems'.", () => {
    const dummyEntries = [
      {
        title: "Boat",
        _id: "5099803df3f4948bd2f98300",
      },
      {
        title: "Bliss",
        _id: "5099803df3f4948bd2f98301",
      },
      {
        title: "Bark",
        _id: "5099803df3f4948bd2f98302",
      },
      {
        title: "Blue",
        _id: "5099803df3f4948bd2f98303",
      },
      {
        title: "Bow",
        _id: "5099803df3f4948bd2f98304",
      },
      {
        title: "Burden",
        _id: "5099803df3f4948bd2f98305",
      },
    ];

    store.state.currentEntries = dummyEntries;

    expect(wrapper.vm.searchItems).toStrictEqual(dummyEntries);
  });

  it("'searchInput' should exist.", () => {
    const searchInput = wrapper.find("#searchInput");
    expect(searchInput.exists()).toBe(true);
  });

  it("Should enter 'b' into the searchInput.", () => {
    const searchInput = wrapper.find("#searchInput");
    searchInput.setValue("b");

    expect(searchInput.element.value).toBe("b");
  });

  it("Should click on 'Bark'", async () => {
    const searchResults = wrapper.findAll(".searchResult");
    expect(searchResults.exists()).toBe(true);

    await searchResults.at(2).trigger("mousedown");
  });

  it("Should have dispatched 'setSelectedEntryById' with id of 'Bark'.", () => {
    expect(actions.setSelectedEntryById.mock.calls[0][1]).toBe(
      "5099803df3f4948bd2f98302"
    );
  });

  it("Max. x num of items of the results should be rendered.", () => {
    wrapper.setData({ maxSearchResults: 4 });

    const searchResults = wrapper.findAll(".searchResult");
    expect(searchResults.exists()).toBe(true);
    expect(searchResults.length).toBe(5);
  });
});
