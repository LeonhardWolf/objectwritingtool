import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { cloneDeep } from "lodash";
import _ from "lodash";

import EntryTitle from "@/components/EntryTitle.vue";
import storeConfig from "../../src/store/modules/all";
import { expectedGETEntries, GETResBody } from "../../src/mocks/Entries";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

const setupMockStore = () => {
  const actions = {
    updateSingleField: jest.fn(),
    addSingleField: jest.fn(),
    deleteLastField: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(EntryTitle, {
    store,
    localVue,
  });
};

describe("Rendering of the title:", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Should render an empty string when there is no title data.", async () => {
    const h1 = wrapper.findAll("#entryTitle");

    expect(h1.length).toBe(1);
    expect(h1.at(0).text()).toBe("");
  });

  describe("Renders the title when the data exists;", () => {
    it("Should set the state 'selectedEntry'.", async () => {
      store.state.selectedEntry = _.cloneDeep(expectedGETEntries[2]);

      expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[2]);
    });

    it("Should render the title.", async () => {
      const h1 = wrapper.findAll("#entryTitle");

      expect(h1.length).toBe(1);
      expect(h1.at(0).text()).toBe("Fence");
    });
  });
});

describe("Rendering of the translation:", () => {
  const store = setupStore();
  const wrapper = createWrapper(store);

  it("Should render an empty string when there is no title data.", async () => {
    const h1 = wrapper.findAll("#entryTranslation");

    expect(h1.length).toBe(1);
    expect(h1.at(0).text()).toBe("");
  });

  describe("Renders the title when the data exists;", () => {
    it("Should set the state 'selectedEntry'.", async () => {
      store.state.selectedEntry = _.cloneDeep(expectedGETEntries[2]);

      expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[2]);
    });

    it("Should render the title.", async () => {
      const h1 = wrapper.findAll("#entryTranslation");

      expect(h1.length).toBe(1);
      expect(h1.at(0).text()).toBe("Zaun");
    });
  });
});
