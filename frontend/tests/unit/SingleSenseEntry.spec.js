import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";

import SingleSenseEntry from "@/components/SingleSenseEntry.vue";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const actions = {
    updateSingleField: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(SingleSenseEntry, {
    propsData: {
      currentEntry: {
        _id: "5099803df3f4948bd2f97310",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
    },
    store,
    localVue,
  });
};

describe("Changes of text in textInput should be dispatched to 'updateSingleField'", () => {
  const [store, actions] = setupStore();
  const wrapper = createWrapper(store);

  const textInput = wrapper.findAll("#textInput");

  it("The textInput should have the value given in props.", () => {
    expect(textInput.exists()).toBe(true);
    expect(textInput.length).toBe(1);
    expect(textInput.at(0).element.value).toBe("I belong to bucket.");
  });

  it("The value of textInput should be updated properly when writing in it.", () => {
    textInput.setValue("I got updated once, bucket.");
    expect(textInput.at(0).element.value).toBe("I got updated once, bucket.");
  });

  it("The updated value of textInput should be dispatched to 'updateSingleField'.", async () => {
    expect(actions.updateSingleField).toHaveBeenCalled();
    expect(actions.updateSingleField.mock.calls[0][1]).toStrictEqual({
      _id: "5099803df3f4948bd2f97310",
      bodyText: "I got updated once, bucket.",
    });
  });
});
