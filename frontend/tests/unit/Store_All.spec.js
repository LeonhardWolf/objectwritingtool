import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import storeConfig from "../../src/store/modules/all";
import { cloneDeep } from "lodash";
import axios from "axios";
import _ from "lodash";
import sinon, { clock } from "sinon";

import { expectedGETEntries, GETResBody } from "../../src/mocks/Entries";
import all from "../../src/store/modules/all";

// const setupMockStore = () => {
//   const state = { ..._.cloneDeep(all.state) };
//   const getters =
//   const store = new Vuex.Store({
//     actions,
//   });
//   return [store, getters, actions];
// };

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

///// Getters ///////////////////////////////////////////

describe("Getter 'getServerReqFailed':", () => {
  const store = setupStore();

  it("'serverReqFailed' should initially be 'false'.", () => {
    expect(store.state.serverReqFailed).toBe(false);
  });

  it("Should return 'false'.", () => {
    expect(store.getters.getServerReqFailed).toBe(false);
  });
});

describe("Getter 'getServerReqFinished':", () => {
  const store = setupStore();

  it("'serverReqFinished' should initially be 'false'.", () => {
    expect(store.state.serverReqFinished).toBe(false);
  });

  it("Should return 'false'.", () => {
    expect(store.getters.getServerReqFinished).toBe(false);
  });
});

describe("Getter 'getCurrentEntries':", () => {
  const store = setupStore();

  it("Should set 'currentEntries'.", async () => {
    store.state.currentEntries = _.cloneDeep(GETResBody);

    expect(store.state.currentEntries).toStrictEqual(GETResBody);
  });

  it("Should return the state 'currentEntries'.", () => {
    expect(store.getters.getCurrentEntries).toStrictEqual(GETResBody);
  });
});

describe("Getter 'getSelectedEntryTitle':", () => {
  const store = setupStore();

  it("Should set 'currentEntries' and 'selectedEntry'.", async () => {
    store.state.currentEntries = _.cloneDeep(GETResBody);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

    expect(store.state.currentEntries).toStrictEqual(GETResBody);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
  });

  it("Should return the title of the selected entry.", async () => {
    const title = await store.getters.getSelectedEntryTitle;
    expect(title).toBe("Table");
  });
});

describe("Getter 'getSelectedEntryTranslation':", () => {
  const store = setupStore();

  it("Should set 'currentEntries' and 'selectedEntry'.", async () => {
    store.state.currentEntries = _.cloneDeep(GETResBody);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

    expect(store.state.currentEntries).toStrictEqual(GETResBody);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
  });

  it("Should return the translation of the selected entry.", async () => {
    const title = await store.getters.getSelectedEntryTranslation;
    expect(title).toBe("Tisch");
  });
});

describe("Getter 'getSelectedEntryFields':", () => {
  const store = setupStore();

  it("Should set 'currentEntries' and 'selectedEntry'.", async () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[1]);

    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[1]);
  });

  it("Should return the fields of the entry with the 'selectedEntryId'.", () => {
    expect(store.getters.getSelectedEntryFields).toStrictEqual(
      expectedGETEntries[1].fields
    );
  });
});

describe("Getter 'getTimerExpanded':", () => {
  const store = setupStore();

  it("Should set states 'expanded' for all widgets.", () => {
    store.state.timerExpanded = true;
    store.state.calendarExpanded = false;
    store.state.searchBarExpanded = false;

    expect(store.state.timerExpanded).toBe(true);
    expect(store.state.calendarExpanded).toBe(false);
    expect(store.state.searchBarExpanded).toBe(false);
  });

  it("Should return 'true' for getter 'getTimerExpanded'.", async () => {
    const getterValue = await store.getters.getTimerExpanded;
    expect(getterValue).toBe(true);
  });
});

describe("Getter 'getCalendarExpanded':", () => {
  const store = setupStore();

  it("Should set states 'expanded' for all widgets.", () => {
    store.state.timerExpanded = false;
    store.state.calendarExpanded = true;
    store.state.searchBarExpanded = false;

    expect(store.state.timerExpanded).toBe(false);
    expect(store.state.calendarExpanded).toBe(true);
    expect(store.state.searchBarExpanded).toBe(false);
  });

  it("Should return 'true' for getter 'getTimerExpanded'.", async () => {
    const getterValue = await store.getters.getCalendarExpanded;
    expect(getterValue).toBe(true);
  });
});

describe("Getter 'getSearchBarExpanded':", () => {
  const store = setupStore();

  it("Should set states 'expanded' for all widgets.", () => {
    store.state.timerExpanded = false;
    store.state.calendarExpanded = false;
    store.state.searchBarExpanded = true;

    expect(store.state.timerExpanded).toBe(false);
    expect(store.state.calendarExpanded).toBe(false);
    expect(store.state.searchBarExpanded).toBe(true);
  });

  it("Should return 'true' for getter 'getTimerExpanded'.", async () => {
    const getterValue = await store.getters.getSearchBarExpanded;
    expect(getterValue).toBe(true);
  });
});

///// Actions ///////////////////////////////////////////

describe("When action 'fetchEntries' is dispatched the HTTP res.data should be saved in the state 'GETEntries', 'currentEntries' and 'selectedEntry'.", () => {
  const store = setupStore();
  const getResBody = _.cloneDeep(GETResBody);

  it("'GETEntries', 'currentEntries' and 'selectedEntry' hold no data to begin with.", () => {
    expect(store.state.GETEntries).toStrictEqual([]);
    expect(store.state.currentEntries).toStrictEqual([]);
    expect(store.state.selectedEntry).toStrictEqual([]);
  });

  it("After dispatch of 'fetchEntries' 'GETEntries' and 'currentEntries' should be filled with the HTTP res.data.", async () => {
    jest.spyOn(axios, "get").mockImplementationOnce(() => {
      const res = {
        data: getResBody,
      };
      return Promise.resolve(res);
    });

    await store.dispatch("fetchEntries");
    expect(axios.get).toHaveBeenCalled();

    expect(store.state.GETEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("Should have set 'selectedEntry' to the current -> the last entry of the HTTP.data array.", () => {
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[2]);
  });

  it("'serverReqFailed' should be 'false'.", () => {
    expect(store.state.serverReqFailed).toBe(false);
  });

  it("'serverReqFinished' should be 'true'.", () => {
    expect(store.state.serverReqFinished).toBe(true);
  });

  describe("Failed GET request changes 'serverReqFailed':", () => {
    const store = setupStore();

    it("'GETEntries', 'currentEntries' and 'selectedEntry' hold no data to begin with.", () => {
      expect(store.state.GETEntries).toStrictEqual([]);
      expect(store.state.currentEntries).toStrictEqual([]);
      expect(store.state.selectedEntry).toStrictEqual([]);
    });

    it("Should return a rejected Promise for 'axios.get()'.", async () => {
      jest.spyOn(axios, "get").mockImplementationOnce(() => {
        return Promise.reject("Some error message");
      });
      jest.spyOn(global.console, "error").mockImplementationOnce(() => {});

      await store.dispatch("fetchEntries");
      expect(axios.get).toHaveBeenCalled();
      expect(global.console.error).toHaveBeenCalledWith("Some error message");
    });

    it("Should not have changed 'GETEntries' and 'currentEntries'.", () => {
      expect(store.state.GETEntries).toStrictEqual([]);
      expect(store.state.currentEntries).toStrictEqual([]);
    });

    it("'serverReqFailed' should be 'true'.", () => {
      expect(store.state.serverReqFailed).toBe(true);
    });

    it("'serverReqFinished' should be 'true'.", () => {
      expect(store.state.serverReqFinished).toBe(true);
    });
  });
});

describe("Action 'updateSingleField' should change the corresponding field in state 'currentEntries'.", () => {
  const store = setupStore();
  const getResBody = _.cloneDeep(GETResBody);

  it("Should fill 'GETEntries' and 'currentEntries' with the HTTP res.data.", async () => {
    jest.spyOn(axios, "get").mockImplementationOnce(() => {
      const res = {
        data: getResBody,
      };
      return Promise.resolve(res);
    });

    await store.dispatch("fetchEntries");
    expect(axios.get).toHaveBeenCalled();

    expect(store.state.GETEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("The field to update should get inserted into the state 'currentEntries' when dispatching 'updateSingleField'.", async () => {
    await store.dispatch("updateSingleField", {
      _id: "5099803df3f4948bd2f97310",
      bodyText: "I got updated once, bucket.",
    });

    expect(store.state.currentEntries[0]).toStrictEqual({
      __v: 0,
      _id: "5099803df3f4948bd2f98300",
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [
        {
          _id: "5099803df3f4948bd2f97310",
          sense: "seeing",
          bodyText: "I got updated once, bucket.",
        },
        {
          _id: "5099803df3f4948bd2f97311",
          sense: "hearing",
          bodyText: "I belong to bucket, too.",
        },
      ],
    });

    expect(store.state.currentEntries[1]).toStrictEqual(expectedGETEntries[1]);
    expect(store.state.currentEntries[2]).toStrictEqual(expectedGETEntries[2]);
  });

  it("Should not have changed 'GETEntries'.", () => {
    expect(store.state.GETEntries).toStrictEqual(expectedGETEntries);
  });
});

describe("Action 'updateSingleField' for the last field of the current entry should commit 'addSingleField'.", () => {
  const setupMockStore = () => {
    const mutations = {
      updateSingleField: jest.fn(),
      addSingleField: jest.fn(),
    };
    const store = new Vuex.Store({
      state: _.cloneDeep(all).state,
      getters: _.cloneDeep(all).getters,
      actions: {
        updateSingleField: _.cloneDeep(all).actions.updateSingleField,
        schedulePUTRequest: jest.fn(),
      },
      mutations,
    });
    return [store, mutations];
  };
  const [store, mutations] = setupMockStore();

  it("'updateSingleField' for the DB filled last textInput should commit 'addSingleField' with the selected entry id.", async () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);

    await store.dispatch("updateSingleField", {
      _id: "5099803df3f4948bd2f97311", // field id
      bodyText: "I got updated once, bucket.",
    });

    expect(mutations.updateSingleField).toHaveBeenCalled();

    expect(mutations.addSingleField).toHaveBeenCalled();
    expect(mutations.addSingleField.mock.calls[0][1]).toBe(
      "5099803df3f4948bd2f98300"
    );
  });

  it("'updateSingleField' from the now added empty and lowest field should commit 'addSingleField' once again.", async () => {
    mutations.updateSingleField.mockReset();
    mutations.addSingleField.mockReset();

    store.state.currentEntries = [
      {
        __v: 0,
        _id: "5099803df3f4948bd2f98300",
        title: "Bucket",
        translation: "Eimer",
        date: new Date("2021-08-17T00:00:00.000Z"),
        fields: [
          {
            _id: "5099803df3f4948bd2f97310",
            sense: "seeing",
            bodyText: "I belong to bucket.",
          },
          {
            _id: "5099803df3f4948bd2f97311",
            sense: "hearing",
            bodyText: "I belong to bucket, too.",
          },
          {
            _id: "new[0]",
            sense: "smell", // next sense in the list
            bodyText: "",
          },
        ],
      },
    ];

    await store.dispatch("updateSingleField", {
      _id: "new[0]", // field id
      bodyText: "I also got updated once, bucket.",
    });

    expect(mutations.updateSingleField).toHaveBeenCalled();

    expect(mutations.addSingleField).toHaveBeenCalled();
    expect(mutations.addSingleField.mock.calls[0][1]).toBe(
      "5099803df3f4948bd2f98300"
    );
  });

  it("'updateSingleField' from a textInput that is not the lowest one should not commit 'addSingleField'.", async () => {
    mutations.updateSingleField.mockReset();
    mutations.addSingleField.mockReset();

    store.state.currentEntries = [
      {
        __v: 0,
        _id: "5099803df3f4948bd2f98300",
        title: "Bucket",
        translation: "Eimer",
        date: new Date("2021-08-17T00:00:00.000Z"),
        fields: [
          {
            _id: "5099803df3f4948bd2f97310",
            sense: "seeing",
            bodyText: "I belong to bucket.",
          },
          {
            _id: "5099803df3f4948bd2f97311",
            sense: "hearing",
            bodyText: "I belong to bucket, too.",
          },
          {
            _id: "new[0]",
            sense: "smell", // next sense in the list
            bodyText: "",
          },
        ],
      },
    ];

    await store.dispatch("updateSingleField", {
      _id: "5099803df3f4948bd2f97311", // field id
      bodyText: "I got updated once again, bucket.",
    });

    expect(mutations.updateSingleField).toHaveBeenCalled();

    expect(mutations.addSingleField).not.toHaveBeenCalled();
  });
});

describe("Deleting all characters in the second last field should dispatch 'deleteLastField'.", () => {
  const setupMockStore = () => {
    const mutations = {
      updateSingleField: jest.fn(),
      deleteLastField: jest.fn(),
    };
    const store = new Vuex.Store({
      state: _.cloneDeep(all).state,
      getters: _.cloneDeep(all).getters,
      actions: {
        updateSingleField: _.cloneDeep(all).actions.updateSingleField,
        schedulePUTRequest: jest.fn(),
      },
      mutations,
    });
    return [store, mutations];
  };
  const [store, mutations] = setupMockStore();

  it("'updateSingleField' with empty string from the second last field should dispatch 'deleteLastField'.", async () => {
    mutations.updateSingleField.mockReset();
    mutations.deleteLastField.mockReset();

    store.state.currentEntries = [
      {
        __v: 0,
        _id: "5099803df3f4948bd2f98300",
        title: "Bucket",
        translation: "Eimer",
        date: new Date("2021-08-17T00:00:00.000Z"),
        fields: [
          {
            _id: "5099803df3f4948bd2f97310",
            sense: "seeing",
            bodyText: "I belong to bucket.",
          },
          {
            _id: "5099803df3f4948bd2f97311",
            sense: "hearing",
            bodyText: "I belong to bucket, too.",
          },
          {
            _id: "new[0]",
            sense: "smell", // next sense in the list
            bodyText: "",
          },
        ],
      },
    ];

    await store.dispatch("updateSingleField", {
      _id: "5099803df3f4948bd2f97311", // field id
      bodyText: "",
    });

    expect(mutations.updateSingleField).toHaveBeenCalled();

    expect(mutations.deleteLastField).toHaveBeenCalled();
    expect(mutations.deleteLastField.mock.calls[0][1]).toBe(
      "5099803df3f4948bd2f98300"
    );
  });

  it("'updateSingleField' with empty string from any other than the second last field should not dispatch 'deleteLastField'.", async () => {
    mutations.updateSingleField.mockReset();
    mutations.deleteLastField.mockReset();

    store.state.currentEntries = [
      {
        __v: 0,
        _id: "5099803df3f4948bd2f98300",
        title: "Bucket",
        translation: "Eimer",
        date: new Date("2021-08-17T00:00:00.000Z"),
        fields: [
          {
            _id: "5099803df3f4948bd2f97310",
            sense: "seeing",
            bodyText: "I belong to bucket.",
          },
          {
            _id: "5099803df3f4948bd2f97311",
            sense: "hearing",
            bodyText: "I belong to bucket, too.",
          },
          {
            _id: "new[0]",
            sense: "smell", // next sense in the list
            bodyText: "",
          },
        ],
      },
    ];

    await store.dispatch("updateSingleField", {
      _id: "5099803df3f4948bd2f97310", // field id
      bodyText: "",
    });

    expect(mutations.updateSingleField).toHaveBeenCalled();

    expect(mutations.deleteLastField).not.toHaveBeenCalled();
  });

  it("A current entry has to hold at least one field at any times.", () => {});
});

describe("Action 'addSingleField' should add an empty field at the end of the proper entry of state array 'currentEntries'.", () => {
  const store = setupStore();
  const getResBody = _.cloneDeep(GETResBody);

  it("Should fill 'GETEntries' and 'currentEntries' with the HTTP res.data.", async () => {
    jest.spyOn(axios, "get").mockImplementationOnce(() => {
      const res = {
        data: getResBody,
      };
      return Promise.resolve(res);
    });

    await store.dispatch("fetchEntries");
    expect(axios.get).toHaveBeenCalled();

    expect(store.state.GETEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("The new empty field should get appended to the proper entry of state array 'currentEntries' when dispatching 'addSingleField'.", async () => {
    await store.dispatch("addSingleField", "5099803df3f4948bd2f98300"); // attribute is entry id

    expect(store.state.currentEntries[0]).toStrictEqual({
      __v: 0,
      _id: "5099803df3f4948bd2f98300",
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [
        {
          _id: "5099803df3f4948bd2f97310",
          sense: "seeing",
          bodyText: "I belong to bucket.",
        },
        {
          _id: "5099803df3f4948bd2f97311",
          sense: "hearing",
          bodyText: "I belong to bucket, too.",
        },
        {
          _id: "new[0]",
          sense: "smell", // next sense in the list
          bodyText: "",
        },
      ],
    });
  });

  it("A second new field should have an ascending '_id'.", async () => {
    await store.dispatch("addSingleField", "5099803df3f4948bd2f98300"); // attribute is entry id

    expect(store.state.currentEntries[0]).toStrictEqual({
      __v: 0,
      _id: "5099803df3f4948bd2f98300",
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [
        {
          _id: "5099803df3f4948bd2f97310",
          sense: "seeing",
          bodyText: "I belong to bucket.",
        },
        {
          _id: "5099803df3f4948bd2f97311",
          sense: "hearing",
          bodyText: "I belong to bucket, too.",
        },
        {
          _id: "new[0]",
          sense: "smell",
          bodyText: "",
        },
        {
          _id: "new[1]", // ascending index
          sense: "taste", // next sense in the list
          bodyText: "",
        },
      ],
    });
  });

  it("All other entries should stay unchanged.", async () => {
    expect(store.state.currentEntries[1]).toStrictEqual(expectedGETEntries[1]);
    expect(store.state.currentEntries[2]).toStrictEqual(expectedGETEntries[2]);
  });
});

describe("Action 'deleteLastField' should delete the last field of the proper entry of state array 'currentEntries'.", () => {
  const store = setupStore();
  const getResBody = _.cloneDeep(GETResBody);

  it("Should fill 'GETEntries' and 'currentEntries' with the HTTP res.data.", async () => {
    jest.spyOn(axios, "get").mockImplementationOnce(() => {
      const res = {
        data: getResBody,
      };
      return Promise.resolve(res);
    });

    await store.dispatch("fetchEntries");
    expect(axios.get).toHaveBeenCalled();

    expect(store.state.GETEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
  });

  it("The last field of to the proper entry of state array 'currentEntries' should get deleted when dispatching 'deleteLastField'.", async () => {
    await store.dispatch("deleteLastField", "5099803df3f4948bd2f98300"); // attribute is entry id

    expect(store.state.currentEntries[0]).toStrictEqual({
      __v: 0,
      _id: "5099803df3f4948bd2f98300",
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [
        {
          _id: "5099803df3f4948bd2f97310",
          sense: "seeing",
          bodyText: "I belong to bucket.",
        },
      ],
    });
  });

  it("All other entries should stay unchanged.", async () => {
    expect(store.state.currentEntries[1]).toStrictEqual(expectedGETEntries[1]);
    expect(store.state.currentEntries[2]).toStrictEqual(expectedGETEntries[2]);
  });
});

describe("Action 'setSelectedEntryByDate':", () => {
  describe("'setSelectedEntryByDate' should set the state 'selectedEntry' accordingly.", () => {
    const store = setupStore();
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    it("Should dispatch 'setSelectedEntry' with date '2021-08-19T01:15:25.000Z'", async () => {
      // const spySetSelectedEntry = jest.spyOn(
      //   store._actions,
      //   "setSelectedEntry"
      // );
      await store.dispatch(
        "setSelectedEntryByDate",
        new Date("2021-08-19T01:15:25.000Z")
      );
      // expect(spySetSelectedEntry).toHaveBeenCalled();
    });

    it("Should have updated 'selectedEntry' to the corresponding entry.", () => {
      expect(store.state.selectedEntry).toStrictEqual(
        _.cloneDeep(expectedGETEntries[2])
      );
    });
  });

  describe("Should leave 'selectedEntry' unchanged when there is no entry with the provided date", () => {
    const store = setupStore();
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);

    it("Should dispatch 'setSelectedEntryByDate' with date '2021-08-30T13:15:04.000Z'", () => {
      // const spySetSelectedEntry = jest.spyOn(
      //   store._actions,
      //   "setSelectedEntry"
      // );

      store.dispatch(
        "setSelectedEntryByDate",
        new Date("2021-08-30T13:15:04.000Z")
      );
      // expect(spySetSelectedEntry).toHaveBeenCalled();
    });

    it("Should not have updated 'selectedEntry'.", () => {
      expect(store.state.selectedEntry).toStrictEqual([]);
    });
  });
});

describe("Action 'setSelectedEntryById':", () => {
  const store = setupStore();

  it("Should have set 'currentEntries' and 'selectedEntry'.", () => {
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    store.state.selectedEntry = _.cloneDeep(expectedGETEntries[2]);

    expect(store.state.currentEntries).toStrictEqual(expectedGETEntries);
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[2]);
  });

  it("Should dispatch 'setSelectedEntryById' with '5099803df3f4948bd2f98300'.", async () => {
    await store.dispatch("setSelectedEntryById", "5099803df3f4948bd2f98300");
    // expect(...).toHaveBeenCalled();
  });

  it("Should have updated 'selectedEntry' properly.", () => {
    expect(store.state.selectedEntry).toStrictEqual(expectedGETEntries[0]);
  });
});

describe("PUT request every 5 secs. after an entry field was changed:", () => {
  it("Action 'updateSingleField' should dispatch 'schedulePUTRequest'.", () => {
    const commit = jest.fn();
    const state = { currentEntries: _.cloneDeep(expectedGETEntries) };
    const dispatch = jest.fn();
    all.actions.updateSingleField(
      { commit, dispatch, state },
      { _id: "5099803df3f4948bd2f98310", bodyText: "Updated again, table." }
    );
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch.mock.calls[0][0]).toBe("schedulePUTRequest");
  });

  describe("Schedule PUT request:", () => {
    const store = setupStore();
    store.state.GETEntries = _.cloneDeep(expectedGETEntries);
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);
    let clock;

    beforeAll(() => {
      clock = sinon.useFakeTimers();
    });
    afterAll(() => {
      clock.restore();
    });

    it("Should not immediately make the PUT request.", async () => {
      jest.spyOn(axios, "put");
      await store.dispatch("schedulePUTRequest");

      expect(axios.put).not.toHaveBeenCalled();
    });

    it("Should not start another setTimeout when being called within these 5000 ms.", async () => {
      await store.dispatch("schedulePUTRequest");
      const runningTimers = Object.keys(clock.timers).length;
      expect(runningTimers).toBe(1); // one setTimeout is still running from the first dispatch of 'schedulePUTRequest'
    });

    it("Should not make the PUT request when being called again after 4999 ms total.", async () => {
      jest.spyOn(axios, "put");

      await store.dispatch("schedulePUTRequest");
      await clock.tickAsync(4999);

      expect(axios.put).not.toHaveBeenCalled();
    });

    it("Should make the PUT request after 5000 ms total without being called.", async () => {
      jest.spyOn(axios, "put").mockImplementationOnce(() => {
        return {
          data: GETResBody,
        };
      });

      await clock.tickAsync(1);

      expect(axios.put).toHaveBeenCalled();
    });

    it("No timers should still be running.", () => {
      const runningTimers = Object.keys(clock.timers).length;
      expect(runningTimers).toBe(0);
    });

    it("Should make another PUT request 5000 ms after it was called again.", async () => {
      jest.spyOn(axios, "put").mockImplementationOnce(() => {
        return {
          data: GETResBody,
        };
      });

      await store.dispatch("schedulePUTRequest");

      await clock.tickAsync(5000);

      expect(axios.put).toHaveBeenCalled();
    });

    it("No timers should still be running.", () => {
      const runningTimers = Object.keys(clock.timers).length;
      expect(runningTimers).toBe(0);
    });

    it("Should not make the PUT request when it's not called.", async () => {
      axios.put.mockReset();
      jest.spyOn(axios, "put");

      await clock.tickAsync(100000);

      expect(axios.put).not.toHaveBeenCalled();
    });
  });

  describe("Should only send the entries on which fields were changed:", () => {
    const store = setupStore();
    store.state.GETEntries = _.cloneDeep(expectedGETEntries);
    store.state.currentEntries = _.cloneDeep(expectedGETEntries);

    let clock;
    beforeAll(() => {
      clock = sinon.useFakeTimers();
    });
    afterAll(() => {
      clock.restore();
    });

    it("No timers should still be running.", () => {
      expect(clock.timers).toBe(undefined);
    });

    it("Should change one 'bodyText' of one field.", async () => {
      store.state.currentEntries[1].fields[0].bodyText =
        "I changed this, table.";
      expect(store.state.currentEntries[1].fields[0].bodyText).toBe(
        "I changed this, table."
      );
    });

    it("Should change another 'bodyText' of one field.", async () => {
      store.state.currentEntries[1].fields[1].bodyText =
        "I changed this too, table.";
      expect(store.state.currentEntries[1].fields[1].bodyText).toBe(
        "I changed this too, table."
      );
    });

    it("Should change another 'bodyText' of another field.", async () => {
      store.state.currentEntries[2].fields[0].bodyText =
        "I changed this, fence.";
      expect(store.state.currentEntries[2].fields[0].bodyText).toBe(
        "I changed this, fence."
      );
    });

    it("Should add one field to an entry.", () => {
      store.state.currentEntries[2].fields.push({
        _id: "new[0]",
        sense: "kinesthetic", // next sense in the list
        bodyText: "",
      });
      expect(store.state.currentEntries[2].fields[2].bodyText).toBe("");
    });

    it("Should send the PUT request with only the changed entries.", async () => {
      const expectedPUTReqBody = [
        {
          __v: 0,
          _id: "5099803df3f4948bd2f98301",
          title: "Table",
          translation: "Tisch",
          date: new Date("2021-08-18T00:00:00.000Z"),
          fields: [
            {
              _id: "5099803df3f4948bd2f98310",
              sense: "smell",
              bodyText: "I changed this, table.",
            },
            {
              _id: "5099803df3f4948bd2f98311",
              sense: "taste",
              bodyText: "I changed this too, table.",
            },
          ],
        },
        {
          __v: 0,
          _id: "5099803df3f4948bd2f98302",
          title: "Fence",
          translation: "Zaun",
          date: new Date("2021-08-19T00:00:00.000Z"),
          fields: [
            {
              _id: "5099803df3f4948bd2f99310",
              sense: "touch",
              bodyText: "I changed this, fence.",
            },
            {
              _id: "5099803df3f4948bd2f99311",
              sense: "organic",
              bodyText: "I belong to fence, too.",
            },
            {
              _id: "new[0]",
              sense: "kinesthetic", // next sense in the list
              bodyText: "",
            },
          ],
        },
      ];

      jest.resetAllMocks();
      jest.spyOn(axios, "put").mockImplementationOnce(() => {
        return { data: _.cloneDeep(expectedGETEntries) };
      });

      await store.dispatch("schedulePUTRequest");
      await clock.tickAsync(5000);

      expect(axios.put).toHaveBeenCalled();
      expect(axios.put.mock.calls[0][1]).toStrictEqual(expectedPUTReqBody);
    });
  });

  describe("Received response from PUT request updates state 'GETEntries':", () => {
    const setupMockStore = () => {
      const mutations = {
        updateSingleField: jest.fn(),
        addSingleField: jest.fn(),
        setGETEntries: _.cloneDeep(all).mutations.setGETEntries,
        setPUTRequestScheduled: jest.fn(),
      };
      const store = new Vuex.Store({
        state: _.cloneDeep(all).state,
        getters: _.cloneDeep(all).getters,
        actions: _.cloneDeep(all).actions,
        mutations,
      });
      return [store, mutations];
    };
    const [store, mutations] = setupMockStore();

    let clock;
    beforeAll(() => {
      clock = sinon.useFakeTimers();
    });
    afterAll(() => {
      clock.restore();
    });

    it("No timers should still be running.", () => {
      expect(clock.timers).toBe(undefined);
    });

    it("Response from PUT request should update state 'GETEntries'.", async () => {
      const fictionalPUTResponse = [
        // this data is completely fictional and not related to any tests before!
        {
          __v: 0,
          _id: "5099803df3f4948bd2f98301",
          title: "Table",
          translation: "Tisch",
          date: "2021-08-18T00:00:00.000Z",
          fields: [
            {
              _id: "5099803df3f4948bd2f98310",
              sense: "smell",
              bodyText: "I belong to table.",
            },
            {
              _id: "5099803df3f4948bd2f98311",
              sense: "taste",
              bodyText: "I belong to table, too.",
            },
          ],
        },
      ];

      const expectedUpdatedGETEntries = [
        {
          __v: 0,
          _id: "5099803df3f4948bd2f98301",
          title: "Table",
          translation: "Tisch",
          date: new Date("2021-08-18T00:00:00.000Z"),
          fields: [
            {
              _id: "5099803df3f4948bd2f98310",
              sense: "smell",
              bodyText: "I belong to table.",
            },
            {
              _id: "5099803df3f4948bd2f98311",
              sense: "taste",
              bodyText: "I belong to table, too.",
            },
          ],
        },
      ];

      jest.spyOn(axios, "put").mockImplementationOnce(() => {
        return { headers: "something", data: fictionalPUTResponse };
      });

      jest.spyOn(mutations, "setGETEntries");

      await store.dispatch("schedulePUTRequest");
      await clock.tickAsync(5000);

      expect(axios.put).toHaveBeenCalled();
      // expect(mutations.setGETEntries).toHaveBeenCalled();
      expect(store.state.GETEntries).toStrictEqual(expectedUpdatedGETEntries);
    });
  });
});

describe("Widget state 'expanded' can only be 'true' for one widget at a time:", () => {
  const store = setupStore();

  // jest.spyOn(all.actions.setWidgetExpanded);

  describe("Expand timer:", () => {
    it("Initial states for 'expanded' of all widgets should be 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should set 'expanded' state for widgets.", () => {
      store.state.timerExpanded = false;
      store.state.calendarExpanded = true;
      store.state.searchBarExpanded = true;

      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(true);
      expect(store.state.searchBarExpanded).toBe(true);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ widget: 'timer', state: true }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "timer",
        state: true,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have only set 'timerExpanded' to 'true'.", () => {
      expect(store.state.timerExpanded).toBe(true);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ widget: 'timer', state: false }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "timer",
        state: false,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have set all 'expanded' states to 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });
  });

  describe("Expand calendar:", () => {
    it("Initial states for 'expanded' of all widgets should be 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should set 'expanded' state for widgets.", () => {
      store.state.timerExpanded = true;
      store.state.calendarExpanded = false;
      store.state.searchBarExpanded = true;

      expect(store.state.timerExpanded).toBe(true);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(true);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ widget: 'calendar', state: true }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "calendar",
        state: true,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have only set 'calendarExpanded' to 'true'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(true);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ calendar: 'timer', state: false }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "calendar",
        state: false,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have set all 'expanded' states to 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });
  });

  describe("Expand searchBar:", () => {
    it("Initial states for 'expanded' of all widgets should be 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should set 'expanded' state for widgets.", () => {
      store.state.timerExpanded = true;
      store.state.calendarExpanded = true;
      store.state.searchBarExpanded = false;

      expect(store.state.timerExpanded).toBe(true);
      expect(store.state.calendarExpanded).toBe(true);
      expect(store.state.searchBarExpanded).toBe(false);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ widget: 'searchBar', state: true }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "searchBar",
        state: true,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have only set 'searchBarExpanded' to 'true'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(true);
    });

    it("Should dispatch 'setWidgetExpanded' with '{ calendar: 'timer', state: false }'.", async () => {
      await store.dispatch("setWidgetExpanded", {
        widget: "calendar",
        state: false,
      });
      // expect(all.actions.setWidgetExpanded).toHaveBeenCalledWith({ timer: true });
    });

    it("Should have set all 'expanded' states to 'false'.", () => {
      expect(store.state.timerExpanded).toBe(false);
      expect(store.state.calendarExpanded).toBe(false);
      expect(store.state.searchBarExpanded).toBe(false);
    });
  });
});

///// Mutations ///////////////////////////////////////////

describe("Mutation 'setGETEntries':", () => {
  const store = setupStore();

  it("State 'GETEntries' should be empty to begin with.", () => {
    expect(store.state.GETEntries).toStrictEqual([]);
  });

  it("Should set the state 'GETEntries' according to its parameters.", async () => {
    const newEntry = [
      {
        __v: 0,
        _id: "5099803df3f4948bd2f98301",
        title: "Table",
        translation: "Tisch",
        date: "2021-08-18T00:00:00.000Z",
        fields: [
          {
            _id: "5099803df3f4948bd2f98310",
            sense: "smell",
            bodyText: "I belong to table.",
          },
          {
            _id: "5099803df3f4948bd2f98311",
            sense: "taste",
            bodyText: "I belong to table, too.",
          },
        ],
      },
    ];

    store.commit("setGETEntries", newEntry);

    expect(store.state.GETEntries).toStrictEqual(newEntry);
  });
});
