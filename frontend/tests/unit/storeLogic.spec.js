import {
  getNextSense,
  getLastNewId,
  getNextNonDbId,
  yearMonthDayEqual,
} from "../../src/store/modules/storeLogic";

describe("Method 'getNextSense' returns the next sense for the given last sense.", () => {
  it("'sight' should return 'hearing'.", async () => {
    expect(getNextSense("sight")).toBe("hearing");
  });

  it("'hearing' should return 'smell'.", async () => {
    expect(getNextSense("hearing")).toBe("smell");
  });

  it("'smell' should return 'taste'.", async () => {
    expect(getNextSense("smell")).toBe("taste");
  });

  it("'taste' should return 'touch'.", async () => {
    expect(getNextSense("taste")).toBe("touch");
  });

  it("'touch' should return 'organic'.", async () => {
    expect(getNextSense("touch")).toBe("organic");
  });

  it("'organic' should return 'kinesthetic'.", async () => {
    expect(getNextSense("organic")).toBe("kinesthetic");
  });

  it("'kinesthetic' should return 'sight'.", async () => {
    expect(getNextSense("kinesthetic")).toBe("sight");
  });
});

describe("Method 'getLastNewId' should return the last id as a Number.", () => {
  it("'new[0]' should return 0.", async () => {
    expect(getLastNewId("new[0]")).toBe(0);
  });

  it("'new[1]' should return 1.", async () => {
    expect(getLastNewId("new[1]")).toBe(1);
  });

  it("'new[10]' should return 10.", async () => {
    expect(getLastNewId("new[10]")).toBe(10);
  });

  it("'new[100]' should return 100.", async () => {
    expect(getLastNewId("new[100]")).toBe(100);
  });
});

describe("Method 'getNextNonDbId' should return the next id for added fields.", () => {
  it("'new[0]' should return 1.", async () => {
    expect(getNextNonDbId("new[0]")).toBe(1);
  });

  it("'new[1]' should return 2.", async () => {
    expect(getNextNonDbId("new[1]")).toBe(2);
  });

  it("'new[10]' should return 11.", async () => {
    expect(getNextNonDbId("new[10]")).toBe(11);
  });

  it("'5099803df3f4948bd2f97311' should return 0.", async () => {
    expect(getNextNonDbId("5099803df3f4948bd2f97311")).toBe(0);
  });
});

describe("'yearMonthDayEqual' should check dates for equality of year, month and date.", () => {
  describe("Return 'true':", () => {
    it("Should return 'true' for '2021-08-19T00:00:00.000Z' and '2021-08-19T00:00:00.000Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-19T00:00:00.000Z"),
        new Date("2021-08-19T00:00:00.000Z")
      );
      expect(result).toBe(true);
    });

    it("Should return 'true' for '2021-08-19T00:00:00.000Z' and '2021-08-19T23:59:59.999Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-19T00:00:00.000Z"),
        new Date("2021-08-19T23:59:59.999Z")
      );
      expect(result).toBe(true);
    });

    it("Should return 'true' for '2021-08-19T23:59:59.999Z' and '2021-08-19T23:59:59.999Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-19T23:59:59.999Z"),
        new Date("2021-08-19T23:59:59.999Z")
      );
      expect(result).toBe(true);
    });

    it("Should return 'true' twice when being called twice.", () => {
      // 'setMinutes()' changes the actual date object
      // calling this several times will consecutively change the minutes

      const date1 = new Date("2021-08-19T23:59:59.999Z");
      const date2 = new Date("2021-08-19T00:00:00.000Z");

      const result1 = yearMonthDayEqual(date1, date2);
      expect(result1).toBe(true);

      const result2 = yearMonthDayEqual(date1, date2);
      expect(result2).toBe(true);
    });
  });

  describe("Return 'false':", () => {
    it("Should return 'false' for '2021-08-20T00:00:00.000Z' and '2021-08-19T00:00:00.000Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-20T00:00:00.000Z"),
        new Date("2021-08-19T00:00:00.000Z")
      );
      expect(result).toBe(false);
    });

    it("Should return 'false' for '2021-08-19T00:00:00.000Z' and '2021-08-20T23:59:59.999Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-19T00:00:00.000Z"),
        new Date("2021-08-20T23:59:59.999Z")
      );
      expect(result).toBe(false);
    });

    it("Should return 'false' for '2021-08-19T23:59:59.999Z' and '2021-08-20T23:59:59.999Z'", () => {
      const result = yearMonthDayEqual(
        new Date("2021-08-19T23:59:59.999Z"),
        new Date("2021-08-20T23:59:59.999Z")
      );
      expect(result).toBe(false);
    });
  });
});
