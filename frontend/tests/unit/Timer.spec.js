import { shallowMount } from "@vue/test-utils";
import Timer from "@/components/Timer.vue";

describe("Timer.vue, zeroPadding", () => {
  const wrapper = shallowMount(Timer);
  it("Pads '9' with one zero in front.", () => {
    const methodZeropadding = wrapper.vm.zeroPadding(9, 2);
    expect(methodZeropadding).toBe("09");
  });

  it("Throws error if inputNumber is not a number.", () => {
    const methodZeropadding = () => wrapper.vm.zeroPadding("hello", 2);
    expect(methodZeropadding).toThrow("inputNumber must be a number.");
  });

  it("Throws error if numberOfDigits is not a number.", () => {
    const methodZeropadding = () => wrapper.vm.zeroPadding(10, "hi");
    expect(methodZeropadding).toThrow("numberOfDigits must be a number.");
  });

  it("Throws error if numberOfDigits is smaller than 1.", () => {
    const methodZeropadding = () => wrapper.vm.zeroPadding(10, 0);
    expect(methodZeropadding).toThrow("numberOfDigits must be at least 1.");
  });

  it("Throws error if numberOfDigits is smaller than the amount of digits of the inputNumber.", () => {
    const methodZeropadding = () => wrapper.vm.zeroPadding(99, 1);
    expect(methodZeropadding).toThrow(
      "numberOfDigits must be higher or equal to the amount of digits of inputNumber."
    );
  });
});

describe("Timer.vue, hrsMinsSecsMillisToMillis", () => {
  const wrapper = shallowMount(Timer);
  it("1h 17m 0s 548ms returns 4620548 milliseconds.", () => {
    // 1h = 1*60*60*1000 ms = 3600000 ms
    // 17m = 17*60*1000 ms = 1020000 ms
    // 0s = 0*1000 ms = 0 ms
    // milliseconds = 1h + 17m + 59s + 548 ms = 3600000 ms + 1020000 ms + 0 ms + 548 ms
    // milliseconds = 4620548
    const methodReturn = wrapper.vm.hrsMinsSecsMillisToMillis([1, 17, 59, 548]);
    expect(methodReturn).toBe(4679548);
  });

  it("timeArray with less or more than 4 entries throws an Error.", () => {
    const method = () => wrapper.vm.hrsMinsSecsMillisToMillis([20, 48, 2]);
    expect(method).toThrow(
      "There must be 4 entries in the timeArray: [hrs, mins, secs, millis]."
    );
  });

  it("Throws a TypeError if not all entries of timeArray are convertible to a number.", () => {
    const method = () => wrapper.vm.hrsMinsSecsMillisToMillis([20, true, 0, 0]);
    expect(method).toThrow(
      "All entries of timeArray must be of type number or convertible to a number."
    );
  });

  it("Throws an Error if not all entries of timeArray are greater or equal to 0.", () => {
    const method = () =>
      wrapper.vm.hrsMinsSecsMillisToMillis([-1, -56, 150, 155]);
    expect(method).toThrow(
      "All entries of timeArray must be greater or equal to 0."
    );
  });
});
