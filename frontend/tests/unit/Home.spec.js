import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { cloneDeep } from "lodash";
import storeConfig from "../../src/store/modules/all";

import Home from "@/views/Home.vue";
import { allEntries } from "../../src/mocks/Entries";

const localVue = createLocalVue();
localVue.use(Vuex);

const setupStore = () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  const store = new Vuex.Store(cloneDeep(storeConfig));
  return store;
};

const setupMockStore = () => {
  const actions = {
    fetchEntries: jest.fn(),
  };
  const store = new Vuex.Store({
    actions,
  });
  return [store, actions];
};

const createWrapper = (store) => {
  return mount(Home, {
    store,
    localVue,
  });
};

describe("Action 'fetchEntries' called when mounted.", () => {
  const [store, actions] = setupMockStore();

  it("Should mount the wrapper.", () => {
    const wrapper = createWrapper(store);
  });

  it("Should have called 'fetchEntries'.", () => {
    expect(actions.fetchEntries).toHaveBeenCalled();
  });
});

describe("Display of 'body', 'loadingData' or 'loadingFailed':", () => {
  describe("Should render 'body':", () => {
    const setupMockStore = () => {
      const state = cloneDeep(storeConfig.state);
      const getters = cloneDeep(storeConfig.getters);
      const actions = {
        fetchEntries: jest.fn(),
      };
      const mutations = cloneDeep(storeConfig.mutations);

      const store = new Vuex.Store({
        state,
        getters,
        actions,
        mutations,
      });
      return [store, actions];
    };

    const [store, actions] = setupMockStore();
    const wrapper = createWrapper(store);

    it("Should set 'serverReqFailed' to 'false'.", () => {
      store.state.serverReqFailed = false;
      expect(store.state.serverReqFailed).toBe(false);
    });

    it("Should set 'serverReqFinished' to 'true'.", () => {
      store.state.serverReqFinished = true;
      expect(store.state.serverReqFinished).toBe(true);
    });

    it("Should render id 'bodyElement'.", () => {
      expect(wrapper.find("#bodyElement").exists()).toBe(true);
    });

    it("Should not render 'loadingDataContainer'.", () => {
      expect(wrapper.find("#loadingDataContainer").exists()).toBe(false);
    });

    it("Should not render 'loadingFailedContainer'.", () => {
      expect(wrapper.find("#loadingFailedContainer").exists()).toBe(false);
    });
  });

  describe("Should render 'loadingData':", () => {
    const setupMockStore = () => {
      const state = cloneDeep(storeConfig.state);
      const getters = cloneDeep(storeConfig.getters);
      const actions = {
        fetchEntries: jest.fn(),
      };
      const mutations = cloneDeep(storeConfig.mutations);

      const store = new Vuex.Store({
        state,
        getters,
        actions,
        mutations,
      });
      return [store, actions];
    };

    const [store, actions] = setupMockStore();
    const wrapper = createWrapper(store);

    it("Should set 'serverReqFailed' to 'false'.", () => {
      store.state.serverReqFailed = false;
      expect(store.state.serverReqFailed).toBe(false);
    });

    it("Should set 'serverReqFinished' to 'false'.", () => {
      store.state.serverReqFinished = false;
      expect(store.state.serverReqFinished).toBe(false);
    });

    it("Should not render id 'bodyElement'.", () => {
      expect(wrapper.find("#bodyElement").exists()).toBe(false);
    });

    it("Should render 'loadingDataContainer'.", () => {
      expect(wrapper.find("#loadingDataContainer").exists()).toBe(true);
    });

    it("Should not render 'loadingFailedContainer'.", () => {
      expect(wrapper.find("#loadingFailedContainer").exists()).toBe(false);
    });
  });

  describe("Should render 'loadingFailed':", () => {
    const setupMockStore = () => {
      const state = cloneDeep(storeConfig.state);
      const getters = cloneDeep(storeConfig.getters);
      const actions = {
        fetchEntries: jest.fn(),
      };
      const mutations = cloneDeep(storeConfig.mutations);

      const store = new Vuex.Store({
        state,
        getters,
        actions,
        mutations,
      });
      return [store, actions];
    };

    const [store, actions] = setupMockStore();
    const wrapper = createWrapper(store);

    it("Should set 'serverReqFailed' to 'true'.", () => {
      store.state.serverReqFailed = true;
      expect(store.state.serverReqFailed).toBe(true);
    });

    it("Should set 'serverReqFinished' to 'true'.", () => {
      store.state.serverReqFinished = true;
      expect(store.state.serverReqFinished).toBe(true);
    });

    it("Should not render id 'bodyElement'.", () => {
      expect(wrapper.find("#bodyElement").exists()).toBe(false);
    });

    it("Should not render 'loadingDataContainer'.", () => {
      expect(wrapper.find("#loadingDataContainer").exists()).toBe(false);
    });

    it("Should render 'loadingFailedContainer'.", () => {
      expect(wrapper.find("#loadingFailedContainer").exists()).toBe(true);
    });
  });
});
