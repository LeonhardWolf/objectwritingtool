const sinon = require("sinon");
const mongoose = require("mongoose");
const _ = require("lodash");
const rewire = require("rewire");

const entriesRewire = rewire("../../routes/api/entries.js");
const testDbSetup = require("./testDbSetup");
const mongooseSchemas = require("../../routes/api/mongooseSchemas");
const dailyObjects = require("../../middleware/dailyObjects");

const app = require("../../app");

const request = require("supertest");

let connection;
let db;

beforeAll(async () => {
  db = await mongoose.connection;
  connection = await mongoose.connect(
    "mongodb://localhost:27017/testDatabase",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => console.log("Connected to db"));
});

afterAll(async () => {
  await db.dropDatabase();
  await db.close();
});

const fillTestDatabase = async () => {
  const expectedEntries = testDbSetup.expectedEntries;
  const entry1 = new mongooseSchemas.Entry({
    _id: mongoose.Types.ObjectId("000000000000000000000000"),
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: [
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000001"),
        sense: "seeing",
        bodyText: "I belong to bucket.",
      }),
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000002"),
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      }),
    ],
  });
  const entry2 = new mongooseSchemas.Entry({
    _id: mongoose.Types.ObjectId("000000000000000000000010"),
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: [
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000011"),
        sense: "smell",
        bodyText: "I belong to table.",
      }),
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000012"),
        sense: "taste",
        bodyText: "I belong to table, too.",
      }),
    ],
  });
  const entry3 = new mongooseSchemas.Entry({
    _id: mongoose.Types.ObjectId("000000000000000000000020"),
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: [
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000021"),
        sense: "touch",
        bodyText: "I belong to fence.",
      }),
      new mongooseSchemas.Field({
        _id: mongoose.Types.ObjectId("000000000000000000000022"),
        sense: "organic",
        bodyText: "I belong to fence, too.",
      }),
    ],
  });

  await mongooseSchemas.Entry.create([entry1, entry2, entry3]);

  const dbEntries = await mongooseSchemas.Entry.find({})
    .sort({ date: "asc" })
    .lean();

  expect(dbEntries).toStrictEqual(expectedEntries);
};

it("Should fill the test database.", async () => {
  await fillTestDatabase();
});

describe("Method 'dateZeroOutTime':", () => {
  it("Should zero out time for '2001-09-25T23:25:18.000Z'", () => {
    // last number of year is one digit

    const startDate = new Date("2001-09-25T23:25:18.000Z");
    const dateZeroOutTime = entriesRewire.__get__("dateZeroOutTime");
    const dateWithoutTime = dateZeroOutTime(startDate);

    expect(dateWithoutTime).toEqual(new Date("2001-09-25T00:00:00.000Z"));
  });

  it("Should zero out time for '2021-09-25T09:25:18.999Z'", () => {
    // month is one digit

    const startDate = new Date("2021-09-25T09:25:18.999Z");
    const dateZeroOutTime = entriesRewire.__get__("dateZeroOutTime");
    const dateWithoutTime = dateZeroOutTime(startDate);

    expect(dateWithoutTime).toEqual(new Date("2021-09-25T00:00:00.000Z"));
  });

  it("Should zero out time for '2021-10-02T15:58:19.499Z'.", () => {
    // day is one digit

    const startDate = new Date("2021-10-02T15:58:19.499Z");
    const dateZeroOutTime = entriesRewire.__get__("dateZeroOutTime");
    const dateWithoutTime = dateZeroOutTime(startDate);

    expect(dateWithoutTime).toEqual(new Date("2021-10-02T00:00:00.000Z"));
  });
});

describe("'./api/entries' GET request.", () => {
  beforeAll(() => {
    this.clock = sinon.useFakeTimers();
  });
  afterAll(() => {
    this.clock.restore();
  });

  it("Should set the system date to '2021-08-19T00:00:00.000Z'.", () => {
    sinon.useFakeTimers(new Date("2021-08-19T00:00:00.000Z"));
    expect(new Date()).toStrictEqual(new Date("2021-08-19T00:00:00.000Z"));
  });

  it("Should successfully return a JSON with all test database entries.", async () => {
    const expectedGETEntries = _.cloneDeep(testDbSetup.expectedGETEntries);

    const res = await request(app).get("/api/entries");

    expect(res.status).toBe(200);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual(expectedGETEntries);
  });

  it("Should send a 503 when the DB returns '[]' for all entries.", async () => {
    jest.spyOn(global.console, "error").mockImplementationOnce(() => {});
    jest
      .spyOn(mongooseSchemas.Entry, "find")
      .mockImplementationOnce((conditions, projection, callback) => {
        const err = "Some error";
        callback(err, []);
        return Promise.resolve("Some value");
      });

    const res = await request(app).get("/api/entries");

    expect(res.status).toBe(503);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual({ err: "Database is not reachable." });
  });

  describe("Add new document with new word object when GET and there is no entry for the date of today:", () => {
    let res = null;

    it("Should set the system date to '2021-08-20T01:15:00.000Z'.", () => {
      sinon.useFakeTimers(new Date("2021-08-20T01:15:00.000Z"));
      expect(new Date()).toStrictEqual(new Date("2021-08-20T01:15:00.000Z"));
    });

    it("Should add a new document because there is none for the set date.", async () => {
      jest
        .spyOn(dailyObjects, "getRandomWordObject")
        .mockImplementationOnce(() => Promise.resolve("Boat"));

      // jest.spyOn(mongooseSchemas.Entry, "create");

      res = await request(app).get("/api/entries");

      expect(dailyObjects.getRandomWordObject).toHaveBeenCalled();
    });

    it("Should have added the new document to the database.", async () => {
      let oldEntries = _.cloneDeep(testDbSetup.expectedEntries);
      oldEntries.push({
        __v: 0,
        _id: expect.any(mongoose.Types.ObjectId),
        title: "Boat",
        translation: "",
        date: new Date("2021-08-20T00:00:00.000Z"),
        fields: [
          {
            _id: expect.any(mongoose.Types.ObjectId),
            sense: "kinesthetic",
            bodyText: "",
          },
        ],
      });
      const expectedEntries = oldEntries;
      const allDbEntries = await mongooseSchemas.Entry.find({})
        .sort({
          date: "asc",
        })
        .lean();

      expect(allDbEntries).toStrictEqual(expectedEntries);
    });

    it("Should send all current Entry documents.", async () => {
      let oldEntries = _.cloneDeep(testDbSetup.expectedGETEntries);
      oldEntries.push({
        __v: 0,
        _id: expect.any(String),
        title: "Boat",
        translation: "",
        date: "2021-08-20T00:00:00.000Z",
        fields: [
          {
            _id: expect.any(String),
            sense: "kinesthetic",
            bodyText: "",
          },
        ],
      });
      const expectedEntries = oldEntries;

      expect(res.status).toBe(201);
      expect(res.header["content-type"]).toBe(
        "application/json; charset=utf-8"
      );
      expect(res.body).toStrictEqual(expectedEntries);
    });
  });
  describe("Add new document with new word object when GET and there is no entry at all:", () => {
    let res = null;

    it("Should not have any entries in DB.", async () => {
      await mongooseSchemas.Entry.deleteMany({});
      const allDbEntries = await mongooseSchemas.Entry.find({});
      expect(allDbEntries).toStrictEqual([]);
    });

    it("Should set the system date to '2021-08-20T01:15:00.000Z'.", () => {
      sinon.useFakeTimers(new Date("2021-08-20T01:15:00.000Z"));
      expect(new Date()).toStrictEqual(new Date("2021-08-20T01:15:00.000Z"));
    });

    it("Should add a new document because there is none for the set date.", async () => {
      jest
        .spyOn(dailyObjects, "getRandomWordObject")
        .mockImplementationOnce(() => Promise.resolve("Boat"));

      // jest.spyOn(mongooseSchemas.Entry, "create");

      res = await request(app).get("/api/entries");

      expect(dailyObjects.getRandomWordObject).toHaveBeenCalled();
    });

    it("Should have added the new document to the database.", async () => {
      const expectedEntries = [
        {
          __v: 0,
          _id: expect.any(mongoose.Types.ObjectId),
          title: "Boat",
          translation: "",
          date: new Date("2021-08-20T00:00:00.000Z"),
          fields: [
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "sight",
              bodyText: "",
            },
          ],
        },
      ];
      const allDbEntries = await mongooseSchemas.Entry.find({})
        .sort({
          date: "asc",
        })
        .lean();

      expect(allDbEntries).toStrictEqual(expectedEntries);
    });

    it("Should send all current Entry documents.", async () => {
      const expectedEntries = [
        {
          __v: 0,
          _id: expect.any(String),
          title: "Boat",
          translation: "",
          date: "2021-08-20T00:00:00.000Z",
          fields: [
            {
              _id: expect.any(String),
              sense: "sight",
              bodyText: "",
            },
          ],
        },
      ];

      expect(res.status).toBe(201);
      expect(res.header["content-type"]).toBe(
        "application/json; charset=utf-8"
      );
      expect(res.body).toStrictEqual(expectedEntries);
    });
  });

  describe("Should not add a new document when there is already one for the date today:", () => {
    let res = null;

    it("Should reset the test database.", async () => {
      await mongooseSchemas.Entry.deleteMany({}, {}, (err) => {
        if (err) return console.error(err);
      });
      await fillTestDatabase();
    });

    it("Should set the system date to '2021-08-19T01:15:00.000Z'.", () => {
      sinon.useFakeTimers(new Date("2021-08-19T01:15:00.000Z"));
      expect(new Date()).toStrictEqual(new Date("2021-08-19T01:15:00.000Z"));
    });

    it("Should not add a new document.", async () => {
      jest.restoreAllMocks();

      jest
        .spyOn(dailyObjects, "getRandomWordObject")
        .mockImplementationOnce(() => "Boat");

      jest.spyOn(mongooseSchemas.Entry, "create");

      res = await request(app).get("/api/entries");

      expect(dailyObjects.getRandomWordObject).not.toHaveBeenCalled();
      expect(mongooseSchemas.Entry.create).not.toHaveBeenCalled();
    });

    it("Should send all current Entry documents.", async () => {
      const expectedEntries = _.cloneDeep(testDbSetup.expectedGETEntries);

      expect(res.status).toBe(200);
      expect(res.header["content-type"]).toBe(
        "application/json; charset=utf-8"
      );
      expect(res.body).toStrictEqual(expectedEntries);
    });
  });
});

describe("'./api/entries' PUT request.", () => {
  let res = null;

  it("Should fill the test database.", async () => {
    const entry1Field1 = {
      _id: mongoose.Types.ObjectId("000000000000000000000001"),
      sense: "seeing",
      bodyText: "I belong to bucket.",
    };
    const entry1Field2 = {
      _id: mongoose.Types.ObjectId("000000000000000000000002"),
      sense: "hearing",
      bodyText: "I belong to bucket, too.",
    };
    const entry1Obj = {
      _id: mongoose.Types.ObjectId("000000000000000000000000"),
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [
        new mongooseSchemas.Field(entry1Field1),
        new mongooseSchemas.Field(entry1Field2),
      ],
    };
    const entry1Expected = {
      __v: 0,
      _id: mongoose.Types.ObjectId("000000000000000000000000"),
      title: "Bucket",
      translation: "Eimer",
      date: new Date("2021-08-17T00:00:00.000Z"),
      fields: [entry1Field1, entry1Field2],
    };
    const Entry1 = new mongooseSchemas.Entry(entry1Obj);

    const entry2Field1 = {
      _id: mongoose.Types.ObjectId("000000000000000000000011"),
      sense: "smell",
      bodyText: "I belong to table.",
    };
    const entry2Field2 = {
      _id: mongoose.Types.ObjectId("000000000000000000000012"),
      sense: "taste",
      bodyText: "I belong to table, too.",
    };
    const entry2Field3 = {
      _id: mongoose.Types.ObjectId("000000000000000000000013"),
      sense: "taste",
      bodyText: "Yep, me too, table.",
    };
    const entry2Obj = {
      _id: mongoose.Types.ObjectId("000000000000000000000010"),
      title: "Table",
      translation: "Tisch",
      date: new Date("2021-08-18T00:00:00.000Z"),
      fields: [
        new mongooseSchemas.Field(entry2Field1),
        new mongooseSchemas.Field(entry2Field2),
        new mongooseSchemas.Field(entry2Field3),
      ],
    };
    const entry2Expected = {
      __v: 0,
      _id: mongoose.Types.ObjectId("000000000000000000000010"),
      title: "Table",
      translation: "Tisch",
      date: new Date("2021-08-18T00:00:00.000Z"),
      fields: [entry2Field1, entry2Field2, entry2Field3],
    };
    const Entry2 = new mongooseSchemas.Entry(entry2Obj);

    const entry3Field1 = {
      _id: mongoose.Types.ObjectId("000000000000000000000021"),
      sense: "touch",
      bodyText: "I belong to fence.",
    };
    const entry3Field2 = {
      _id: mongoose.Types.ObjectId("000000000000000000000022"),
      sense: "organic",
      bodyText: "I belong to fence, too.",
    };
    const entry3Obj = {
      __v: 0,
      _id: mongoose.Types.ObjectId("000000000000000000000020"),
      title: "Fence",
      translation: "Zaun",
      date: new Date("2021-08-19T00:00:00.000Z"),
      fields: [
        new mongooseSchemas.Field(entry3Field1),
        new mongooseSchemas.Field(entry3Field2),
      ],
    };
    const entry3Expected = {
      __v: 0,
      _id: mongoose.Types.ObjectId("000000000000000000000020"),
      title: "Fence",
      translation: "Zaun",
      date: new Date("2021-08-19T00:00:00.000Z"),
      fields: [entry3Field1, entry3Field2],
    };

    const Entry3 = new mongooseSchemas.Entry(entry3Obj);

    await db.dropDatabase();

    await mongooseSchemas.Entry.create([Entry1, Entry2, Entry3]);

    const dbEntries = await mongooseSchemas.Entry.find({})
      .sort({ date: "asc" })
      .lean();

    expect(dbEntries).toStrictEqual([
      entry1Expected,
      entry2Expected,
      entry3Expected,
    ]);
  });

  it("Should update the proper fields in the test database.", async () => {
    const expectedPUTEntries = testDbSetup.expectedPUTEntries;

    const putReqBody = [
      {
        _id: "000000000000000000000010",
        title: "Table",
        translation: "Tisch",
        date: "2021-08-18T00:00:00.000Z",
        fields: [
          {
            _id: "000000000000000000000011",
            sense: "smell",
            bodyText: "I changed this, table.",
          },
        ],
      },
      {
        _id: "000000000000000000000020",
        title: "Fence",
        translation: "Zaun",
        date: "2021-08-19T00:00:00.000Z",
        fields: [
          {
            _id: "000000000000000000000021",
            sense: "touch",
            bodyText: "I changed this, fence.",
          },
          {
            _id: "000000000000000000000022",
            sense: "organic",
            bodyText: "I belong to fence, too.",
          },
          {
            _id: "new[0]",
            sense: "kinesthetic", // next sense in the list
            bodyText: "something",
          },
          {
            _id: "new[1]",
            sense: "sight", // next sense in the list
            bodyText: "and something else",
          },
        ],
      },
    ];

    res = await request(app)
      .put("/api/entries")
      .send(putReqBody)
      .set("Accept", "application/json")
      .set("Content-Type", "application/json");

    const dbEntries = await mongooseSchemas.Entry.find({}, (err) => {
      if (err) return console.error(err);
    })
      .sort({ date: "asc" })
      .lean();

    expect(dbEntries).toStrictEqual(expectedPUTEntries);
  });

  it("Should send all current entries back.", async () => {
    expect(res.status).toBe(201);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.header["access-control-allow-origin"]).toBe("*");
    expect(res.body).toStrictEqual(testDbSetup.expectedPUTRes);
  });

  it("Should send a 400 when any req data is wrong.", async () => {
    const badPUTReqBody = [
      {
        _id: "000000000000000000000010",
        title: "Table",
        translation: "Tisch",
        date: "2021-08-18T00:00:00.000Z",
        fields: [
          {
            _id: "000000000000000000000011",
            sense: "smell",
            bodyText: "I got updated, table.",
          },
          {
            _id: "this is not going to work",
            sense: "organic",
            bodyText: "I got updated too, fence.",
          },
        ],
      },
    ];

    jest.spyOn(global.console, "error").mockImplementationOnce(() => {});
    jest.spyOn(mongooseSchemas.Entry, "updateOne");

    res = await request(app)
      .put("/api/entries")
      .send(badPUTReqBody)
      .set("Accept", "application/json")
      .set("Content-Type", "application/json");

    expect(mongooseSchemas.Entry.updateOne).toThrow();

    expect(res.status).toBe(400);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual({
      err: "You sent invalid data. The PUT request could not be processed.",
    });
  });

  it("Should send a 503 when the DB returns '[]' for all entries.", async () => {
    jest
      .spyOn(mongooseSchemas.Entry, "find")
      .mockImplementationOnce((conditions, projection, callback) => {
        // this makes the chained function call call itself until it reaches the last function and returns '[]'
        const find = {
          sort: () => find,
          lean: () => [],
        };
        return find;
      });

    const res = await request(app)
      .put("/api/entries")
      .send([
        {
          _id: "000000000000000000000010",
          title: "Table",
          translation: "Tisch",
          date: "2021-08-18T00:00:00.000Z",
          fields: [
            {
              _id: "000000000000000000000011",
              sense: "smell",
              bodyText: "I changed this, table.",
            },
            {
              _id: "000000000000000000000012",
              sense: "taste",
              bodyText: "I changed this too, table.",
            },
          ],
        },
      ])
      .set("Accept", "application/json")
      .set("Content-Type", "application/json");

    expect(res.status).toBe(503);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual({ err: "Database is not reachable." });
  });

  it("Should send a 503 when the DB method 'find' calls the callback with an err.", async () => {
    jest
      .spyOn(mongooseSchemas.Entry, "find")
      .mockImplementationOnce((conditions, projection, callback) => {
        callback((err = "some error"));

        // this makes the chained function call call itself until it reaches the last function and returns '[]'
        const find = {
          sort: () => find,
          lean: () => ["something"],
        };
        return find;
      });
    jest.spyOn(global.console, "error").mockImplementationOnce(() => {});

    const res = await request(app)
      .put("/api/entries")
      .send([
        {
          _id: "000000000000000000000010",
          title: "Table",
          translation: "Tisch",
          date: "2021-08-18T00:00:00.000Z",
          fields: [
            {
              _id: "000000000000000000000011",
              sense: "smell",
              bodyText: "I changed this, table.",
            },
            {
              _id: "000000000000000000000012",
              sense: "taste",
              bodyText: "I changed this too, table.",
            },
          ],
        },
      ])
      .set("Accept", "application/json")
      .set("Content-Type", "application/json");

    expect(res.status).toBe(503);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual({ err: "Database is not reachable." });
  });

  it("Should send a 503 when the DB method 'findOneAndUpdate' calls the callback with an err.", async () => {
    jest
      .spyOn(mongooseSchemas.Entry, "findOneAndUpdate")
      .mockImplementationOnce((conditions, update, options, callback) => {
        callback((err = "some error"));
      });
    jest.spyOn(global.console, "error").mockImplementationOnce(() => {});

    const res = await request(app)
      .put("/api/entries")
      .send([
        {
          _id: "000000000000000000000010",
          title: "Table",
          translation: "Tisch",
          date: "2021-08-18T00:00:00.000Z",
          fields: [
            {
              _id: "000000000000000000000011",
              sense: "smell",
              bodyText: "I changed this, table.",
            },
            {
              _id: "000000000000000000000012",
              sense: "taste",
              bodyText: "I changed this too, table.",
            },
          ],
        },
      ])
      .set("Accept", "application/json")
      .set("Content-Type", "application/json");

    expect(res.status).toBe(503);
    expect(res.header["content-type"]).toBe("application/json; charset=utf-8");
    expect(res.body).toStrictEqual({ err: "Database is not reachable." });
  });
});
