const mongoose = require("mongoose");
const rewire = require("rewire");

const dailyObjectsRewire = rewire("../../middleware/dailyObjects");
const dailyObjects = require("../../middleware/dailyObjects");

let connection;
let db;

beforeAll(async () => {
  db = await mongoose.connection;
  connection = await mongoose.connect(
    "mongodb://localhost:27017/testDatabase",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => console.log("Connected to db"));
});

afterAll(async () => {
  await db.dropDatabase();
  await db.close();
});

describe("Extract lines from .txt file:", () => {
  it("Should read the .txt file properly.", () => {
    const expectedString = `ATM
CD
SUV
TV
aardvark
abacus
abbey
abbreviation
`;
    const readTxt = dailyObjectsRewire.__get__("readTxt");

    const txtString = readTxt("./tests/unit/nounListExtract.txt", "UTF-8");
    expect(txtString).toBe(expectedString);
  });

  it("Should filter out empty elements of an array.", () => {
    const stringArray = [
      "ATM",
      "CD",
      "SUV",
      "",
      "TV",
      "aardvark",
      "abacus",
      "abbey",
      "abbreviation",
      "",
    ];
    const filterEmptyElements = dailyObjectsRewire.__get__(
      "filterEmptyElements"
    );

    expect(filterEmptyElements(stringArray)).toStrictEqual([
      "ATM",
      "CD",
      "SUV",
      "TV",
      "aardvark",
      "abacus",
      "abbey",
      "abbreviation",
    ]);
  });

  it("Should split the lines into individual elements of an array.", () => {
    const readTxt = dailyObjectsRewire.__get__("readTxt");
    const splitLines = dailyObjectsRewire.__get__("splitLines");

    const txtString = readTxt("./tests/unit/nounListExtract.txt", "UTF-8");
    expect(splitLines(txtString)).toEqual([
      "ATM",
      "CD",
      "SUV",
      "TV",
      "aardvark",
      "abacus",
      "abbey",
      "abbreviation",
      "",
    ]);
  });

  it("Should return the filtered lines of the .txt file.", () => {
    const extractLinesFromTxt = dailyObjectsRewire.__get__(
      "extractLinesFromTxt"
    );
    const filteredTxtArray = extractLinesFromTxt(
      "./tests/unit/nounListExtract.txt",
      "UTF-8"
    );
    expect(filteredTxtArray).toEqual([
      "ATM",
      "CD",
      "SUV",
      "TV",
      "aardvark",
      "abacus",
      "abbey",
      "abbreviation",
    ]);
  });
});

describe("Save array elements into database:", () => {
  it("Database should hold all elements of the array.", async () => {
    await dailyObjects.saveDailyTxtObjects(
      "./tests/unit/nounListExtract.txt",
      "UTF-8"
    );

    const allDailyObjects = await dailyObjects.DailyObject.find(
      {},
      { _id: false, __v: false }, // this prevents the _id and __v from being sent
      (err) => {
        if (err) return console.error(err);
      }
    )
      .sort("object") // returns objects in ascending alphabetical order because they are unsorted in the db
      .lean();
    expect(allDailyObjects).toStrictEqual([
      {
        object: "ATM",
      },
      {
        object: "Aardvark",
      },
      {
        object: "Abacus",
      },
      {
        object: "Abbey",
      },
      {
        object: "Abbreviation",
      },
      {
        object: "CD",
      },
      {
        object: "SUV",
      },
      {
        object: "TV",
      },
    ]);
  });
});

describe("Return one random word object from database:", () => {
  it("Should reset the 'DailyObjects' collection.", async () => {
    await dailyObjects.DailyObject.deleteMany({}, (err) => {
      if (err) return console.error(err);
    });

    const allDailyObjects = await dailyObjects.DailyObject.find({}, (err) => {
      if (err) return console.error(err);
    }).lean();

    expect(allDailyObjects).toStrictEqual([]);
  });

  it("Should fill 'DailyObjects' collection properly.", async () => {
    await dailyObjects.saveDailyTxtObjects(
      "./tests/unit/nounListExtract.txt",
      "UTF-8"
    );

    const allDailyObjects = await dailyObjects.DailyObject.find(
      {},
      { _id: false, __v: false }, // this prevents the _id and __v from being sent
      (err) => {
        if (err) return console.error(err);
      }
    )
      .sort("object") // returns objects in ascending alphabetical order because they are unsorted in the db
      .lean();
    expect(allDailyObjects).toStrictEqual([
      {
        object: "ATM",
      },
      {
        object: "Aardvark",
      },
      {
        object: "Abacus",
      },
      {
        object: "Abbey",
      },
      {
        object: "Abbreviation",
      },
      {
        object: "CD",
      },
      {
        object: "SUV",
      },
      {
        object: "TV",
      },
    ]);
  });

  describe("Should return a random word object from the collection:", () => {
    const wordArray = [
      "ATM",
      "Aardvark",
      "Abacus",
      "Abbey",
      "Abbreviation",
      "CD",
      "SUV",
      "TV",
    ];

    it("Proper word object when 'Math.random() = 0'.", async () => {
      const mathMock = { random: () => 0 };
      const randomWordObject = await dailyObjects.getRandomWordObject(mathMock);
      expect(wordArray.findIndex((word) => word === randomWordObject)).not.toBe(
        -1
      );
    });

    it("Proper word object when 'Math.random() = 0.5'.", async () => {
      const mathMock = { random: () => 0.5 };
      const randomWordObject = await dailyObjects.getRandomWordObject(mathMock);
      expect(wordArray.findIndex((word) => word === randomWordObject)).not.toBe(
        -1
      );
    });

    it("Proper word object when 'Math.random() = 0.999'.", async () => {
      const mathMock = { random: () => 0.999 };
      const randomWordObject = await dailyObjects.getRandomWordObject(mathMock);
      expect(wordArray.findIndex((word) => word === randomWordObject)).not.toBe(
        -1
      );
    });
  });
});

describe("Method 'getNextSense' returns the next sense for the given last sense.", () => {
  it("'sight' should return 'hearing'.", async () => {
    expect(dailyObjects.getNextSense("sight")).toBe("hearing");
  });

  it("'hearing' should return 'smell'.", async () => {
    expect(dailyObjects.getNextSense("hearing")).toBe("smell");
  });

  it("'smell' should return 'taste'.", async () => {
    expect(dailyObjects.getNextSense("smell")).toBe("taste");
  });

  it("'taste' should return 'touch'.", async () => {
    expect(dailyObjects.getNextSense("taste")).toBe("touch");
  });

  it("'touch' should return 'organic'.", async () => {
    expect(dailyObjects.getNextSense("touch")).toBe("organic");
  });

  it("'organic' should return 'kinesthetic'.", async () => {
    expect(dailyObjects.getNextSense("organic")).toBe("kinesthetic");
  });

  it("'kinesthetic' should return 'sight'.", async () => {
    expect(dailyObjects.getNextSense("kinesthetic")).toBe("sight");
  });
});
