const mongoose = require("mongoose");

function setupSchemasAndFields() {
  const testFieldSchema = new mongoose.Schema({
    sense: String,
    bodyText: String,
  });

  const testEntrySchema = new mongoose.Schema({
    title: String,
    translation: String,
    date: Date,
    fields: [testFieldSchema],
  });

  const TestField = mongoose.model("TestField", testFieldSchema);
  const TestEntry = mongoose.model("TestEntry", testEntrySchema);

  return [TestField, TestEntry];
}

const [TestField, TestEntry] = setupSchemasAndFields();

async function saveEntries() {
  const fields1 = [
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f97310"),
      sense: "seeing",
      bodyText: "I belong to bucket.",
    }),
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f97311"),
      sense: "hearing",
      bodyText: "I belong to bucket, too.",
    }),
  ];
  const entry1 = new TestEntry({
    _id: mongoose.Types.ObjectId("5099803df3f4948bd2f98300"),
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: fields1,
  });
  await entry1.save();

  const fields2 = [
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f98310"),
      sense: "smell",
      bodyText: "I belong to table.",
    }),
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f98311"),
      sense: "taste",
      bodyText: "I belong to table, too.",
    }),
  ];
  const entry2 = new TestEntry({
    _id: mongoose.Types.ObjectId("5099803df3f4948bd2f98301"),
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: fields2,
  });
  await entry2.save();

  const fields3 = [
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f99310"),
      sense: "touch",
      bodyText: "I belong to fence.",
    }),
    new TestField({
      _id: mongoose.Types.ObjectId("5099803df3f4948bd2f99311"),
      sense: "organic",
      bodyText: "I belong to fence, too.",
    }),
  ];
  const entry3 = new TestEntry({
    _id: mongoose.Types.ObjectId("5099803df3f4948bd2f98302"),
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: fields3,
  });
  await entry3.save();
}

const expectedEntries = [
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000000"),
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000001"),
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: mongoose.Types.ObjectId("000000000000000000000002"),
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000010"),
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000011"),
        sense: "smell",
        bodyText: "I belong to table.",
      },
      {
        _id: mongoose.Types.ObjectId("000000000000000000000012"),
        sense: "taste",
        bodyText: "I belong to table, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000020"),
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000021"),
        sense: "touch",
        bodyText: "I belong to fence.",
      },
      {
        _id: mongoose.Types.ObjectId("000000000000000000000022"),
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
    ],
  },
];

// HTTP can't send objects, so both ID and date are expected to be strings
const expectedGETEntries = [
  {
    __v: 0,
    _id: "000000000000000000000000",
    title: "Bucket",
    translation: "Eimer",
    date: "2021-08-17T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000001",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: "000000000000000000000002",
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "000000000000000000000010",
    title: "Table",
    translation: "Tisch",
    date: "2021-08-18T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000011",
        sense: "smell",
        bodyText: "I belong to table.",
      },
      {
        _id: "000000000000000000000012",
        sense: "taste",
        bodyText: "I belong to table, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "000000000000000000000020",
    title: "Fence",
    translation: "Zaun",
    date: "2021-08-19T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000021",
        sense: "touch",
        bodyText: "I belong to fence.",
      },
      {
        _id: "000000000000000000000022",
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
    ],
  },
];

const putReqChangeDelBody = [
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98300",
    title: "Bucket",
    translation: "Eimer",
    date: "2021-08-17T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f97310",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: "5099803df3f4948bd2f97311",
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98301",
    title: "Table",
    translation: "Tisch",
    date: "2021-08-18T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f98310",
        sense: "smell",
        bodyText: "I got updated, table.",
      },
      {
        _id: "5099803df3f4948bd2f98311",
        sense: "taste",
        bodyText: "I belong to table, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "5099803df3f4948bd2f98302",
    title: "Fence",
    translation: "Zaun",
    date: "2021-08-19T00:00:00.000Z",
    fields: [
      {
        _id: "5099803df3f4948bd2f99310",
        sense: "touch",
        bodyText: "I belong to fence.",
      },
      {
        _id: "5099803df3f4948bd2f99311",
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
    ],
  },
];

const expectedPUTEntries = [
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000000"),
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000001"),
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: mongoose.Types.ObjectId("000000000000000000000002"),
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000010"),
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000011"),
        sense: "smell",
        bodyText: "I changed this, table.",
      },
    ],
  },
  {
    __v: 0,
    _id: mongoose.Types.ObjectId("000000000000000000000020"),
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: [
      {
        _id: mongoose.Types.ObjectId("000000000000000000000021"),
        sense: "touch",
        bodyText: "I changed this, fence.",
      },
      {
        _id: mongoose.Types.ObjectId("000000000000000000000022"),
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
      {
        _id: expect.any(mongoose.Types.ObjectId),
        sense: "kinesthetic", // next sense in the list
        bodyText: "something",
      },
      {
        _id: expect.any(mongoose.Types.ObjectId),
        sense: "sight", // next sense in the list
        bodyText: "and something else",
      },
    ],
  },
];

const expectedPUTRes = [
  {
    __v: 0,
    _id: "000000000000000000000000",
    title: "Bucket",
    translation: "Eimer",
    date: "2021-08-17T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000001",
        sense: "seeing",
        bodyText: "I belong to bucket.",
      },
      {
        _id: "000000000000000000000002",
        sense: "hearing",
        bodyText: "I belong to bucket, too.",
      },
    ],
  },
  {
    __v: 0,
    _id: "000000000000000000000010",
    title: "Table",
    translation: "Tisch",
    date: "2021-08-18T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000011",
        sense: "smell",
        bodyText: "I changed this, table.",
      },
    ],
  },
  {
    __v: 0,
    _id: "000000000000000000000020",
    title: "Fence",
    translation: "Zaun",
    date: "2021-08-19T00:00:00.000Z",
    fields: [
      {
        _id: "000000000000000000000021",
        sense: "touch",
        bodyText: "ch",
        bodyText: "I changed this, fence.",
      },
      {
        _id: "000000000000000000000022",
        sense: "organic",
        bodyText: "I belong to fence, too.",
      },
      {
        _id: expect.any(String),
        sense: "kinesthetic", // next sense in the list
        bodyText: "something",
      },
      {
        _id: expect.any(String),
        sense: "sight", // next sense in the list
        bodyText: "and something else",
      },
    ],
  },
];

module.exports = {
  saveEntries,
  expectedEntries,
  expectedGETEntries,
  expectedPUTEntries,
  expectedPUTRes,
  TestEntry,
};
