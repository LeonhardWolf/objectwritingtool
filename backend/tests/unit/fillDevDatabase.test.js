const mongoose = require("mongoose");

const mongooseSchemas = require("../../routes/api/mongooseSchemas");
const dailyObjects = require("../../middleware/dailyObjects");
const fillDevDatabase = require("../../dev/fillDevDatabase");

let connection;
let db;

beforeAll(async () => {
  db = await mongoose.connection;
  connection = await mongoose.connect(
    "mongodb://localhost:27017/testDatabase",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => console.log("Connected to db"));
});

afterAll(async () => {
  await db.dropDatabase();
  await db.close();
});

describe("Refill the database:", () => {
  describe("Entries:", () => {
    it("Should fill the database initially with unwanted data.", async () => {
      const unwantedEntry = new mongooseSchemas.Entry({
        title: "House",
        translation: "Haus",
        date: new Date("2021-08-29T00:00:00.000Z"),
        fields: [
          new mongooseSchemas.Field({
            sense: "hearing",
            bodyText: "Something about hearing certainly.",
          }),
        ],
      });
      await unwantedEntry.save();

      const allDocs = await mongooseSchemas.Entry.find({}).lean();

      expect(allDocs).toStrictEqual([
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          title: "House",
          translation: "Haus",
          date: new Date("2021-08-29T00:00:00.000Z"),
          fields: [
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "hearing",
              bodyText: "Something about hearing certainly.",
            },
          ],
        },
      ]);
    });

    it("Should refill the database with the proper data.", async () => {
      await fillDevDatabase.refillDevDatabaseEntries();

      const allDocs = await mongooseSchemas.Entry.find({}).lean();

      expect(allDocs).toStrictEqual([
        {
          __v: 0,
          _id: expect.any(mongoose.Types.ObjectId),
          title: "Bucket",
          translation: "Eimer",
          date: new Date("2021-08-17T00:00:00.000Z"),
          fields: [
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "seeing",
              bodyText: "I belong to bucket.",
            },
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "hearing",
              bodyText: "I belong to bucket, too.",
            },
          ],
        },
        {
          __v: 0,
          _id: expect.any(mongoose.Types.ObjectId),
          title: "Table",
          translation: "Tisch",
          date: new Date("2021-08-18T00:00:00.000Z"),
          fields: [
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "smell",
              bodyText: "I belong to table.",
            },
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "taste",
              bodyText: "I belong to table, too.",
            },
          ],
        },
        {
          __v: 0,
          _id: expect.any(mongoose.Types.ObjectId),
          title: "Fence",
          translation: "Zaun",
          date: new Date("2021-08-19T00:00:00.000Z"),
          fields: [
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "touch",
              bodyText: "I belong to fence.",
            },
            {
              _id: expect.any(mongoose.Types.ObjectId),
              sense: "organic",
              bodyText: "I belong to fence, too.",
            },
          ],
        },
      ]);
    });
  });

  describe("DailyObjects:", () => {
    it("Should fill the database initially with unwanted data.", async () => {
      const newObjectDoc = new dailyObjects.DailyObject({ object: "Water" });
      await newObjectDoc.save();

      const allDocs = await dailyObjects.DailyObject.find({}).lean();
      expect(allDocs).toStrictEqual([
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "Water",
        },
      ]);
    });

    it("Should refill the database with the proper data.", async () => {
      await fillDevDatabase.refillDevDatabaseDailyObjects(
        "./tests/unit/nounListExtract.txt",
        "UTF-8"
      );

      const allDailyObjects = await dailyObjects.DailyObject.find({})
        .sort("object") // returns objects in ascending alphabetical order because they are unsorted in the db
        .lean();

      expect(allDailyObjects).toStrictEqual([
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "ATM",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "Aardvark",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "Abacus",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "Abbey",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "Abbreviation",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "CD",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "SUV",
        },
        {
          _id: expect.any(mongoose.Types.ObjectId),
          __v: 0,
          object: "TV",
        },
      ]);
    });
  });
});
