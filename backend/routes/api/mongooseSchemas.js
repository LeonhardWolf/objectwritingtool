const mongoose = require("mongoose");

const fieldSchema = new mongoose.Schema({
  sense: String,
  bodyText: String,
});

const entrySchema = new mongoose.Schema({
  title: String,
  translation: String,
  date: Date,
  fields: [fieldSchema],
});

const Field = mongoose.model("Field", fieldSchema);
const Entry = mongoose.model("Entry", entrySchema);

module.exports = { Field, Entry };
