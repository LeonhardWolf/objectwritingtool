const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const _ = require("lodash");

const mongooseSchemas = require("./mongooseSchemas");
const dailyObjects = require("../../middleware/dailyObjects");

const db = mongoose.connection;

const connectDB = async () => {
  await mongoose.connect(
    `${process.env.OBJECTWRITINGTOOL_MONGODB_URL}ObjectWritingTool_DB` ||
      `mongodb://localhost:27017/ObjectWritingTool_DB`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
};

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () =>
  console.log(`Connected to MongoDB on: '${db.host}:${db.port}/${db.name}'`)
);

const dbErrorMessage = (res) => {
  res.status(503);
  res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
  res.set({ "Content-Type": "application/json" });
  res.send(JSON.stringify({ err: "Database is not reachable." }));
};

const eraseTimezoneOffset = (dateObject) => {
  const dateClone = new Date(dateObject);
  const dateOffsetMins = dateClone.getTimezoneOffset(); // returns the offset in minutes
  const dateNoTimezoneOffset = new Date(
    dateClone.setMinutes(dateClone.getMinutes() + dateOffsetMins)
  );
  return dateNoTimezoneOffset;
};

const dateZeroOutTime = (dateObject) => {
  const date = eraseTimezoneOffset(dateObject);

  const yearToday = date.getFullYear();
  const monthToday = String(date.getMonth() + 1).padStart(2, "0"); // zero-padding with two digits
  const dayToday = String(date.getDate()).padStart(2, "0");

  const yearMonthDayToday = new Date(
    `${yearToday}-${monthToday}-${dayToday}T00:00:00.000Z`
  );
  return yearMonthDayToday;
};

const createNewDailyEntry = async (dateToday) => {
  const randomWordObject = await dailyObjects.getRandomWordObject();

  const yearMonthDayToday = dateZeroOutTime(dateToday);

  const lastEntry = await mongooseSchemas.Entry.findOne({}, {}, (err) => {
    if (err) return console.error(err);
  }).sort({ date: "desc" }); // find latest entry

  if (!lastEntry) {
    const newDailyEntry = new mongooseSchemas.Entry({
      title: randomWordObject,
      translation: "",
      date: yearMonthDayToday,
      fields: new mongooseSchemas.Field({
        sense: "sight",
        bodyText: "",
      }),
    });

    return await newDailyEntry.save();
  }

  const lastSense = lastEntry.fields[lastEntry.fields.length - 1].sense;

  const nextSense = dailyObjects.getNextSense(lastSense);

  const newDailyEntry = new mongooseSchemas.Entry({
    title: randomWordObject,
    translation: "",
    date: yearMonthDayToday,
    fields: new mongooseSchemas.Field({
      sense: nextSense,
      bodyText: "",
    }),
  });

  await newDailyEntry.save();
};

router.get("/", async (req, res) => {
  let dbErrorOccured = false;
  const dateToday = new Date();

  const entryOfToday = await mongooseSchemas.Entry.find(
    { date: dateZeroOutTime(dateToday) },
    {},
    (err, result) => {
      if (err) {
        dbErrorOccured = true;
        return console.error(err);
      }
    }
  );

  if (dbErrorOccured) return dbErrorMessage(res);

  if (entryOfToday.length === 0) {
    await createNewDailyEntry(dateToday);
    const updatedEntries = await mongooseSchemas.Entry.find({}, {}, (err) => {
      if (err) return console.error(err);
    })
      .sort({ date: "asc" })
      .lean();

    res.status(201);
    res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
    res.set({ "Content-Type": "application/json" });
    res.send(JSON.stringify(updatedEntries));
    return null;
  }

  const entries = await mongooseSchemas.Entry.find({}, {}, (err) => {
    if (err) {
      dbErrorOccured = true;
      return consol.error(err);
    }
  })
    .sort({ date: "asc" })
    .lean();

  if (dbErrorOccured) return dbErrorMessage(res);

  if (entries.length !== 0) {
    res.status(200);
    res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
    res.set({ "Content-Type": "application/json" });
    res.send(JSON.stringify(entries));
  } else {
    res.status(503);
    res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
    res.set({ "Content-Type": "application/json" });
    res.send(JSON.stringify({ err: "Database is not reachable." }));
  }
});

const invalidReqData = (res) => {
  res.status(400);
  res.set({ "Content-Type": "application/json" });
  res.send(
    JSON.stringify(
      "You sent invalid data. The PUT request could not be processed."
    )
  );
};

const indexCleanAllFields = (req) => {
  const indexCleanedPUTEntries = req.body.map((entry) => {
    const cleanedEntry = entry.fields.map((field) => {
      if (field._id.slice(0, 3) === "new") {
        delete field._id;
      }
    });
    return entry;
  });

  return indexCleanedPUTEntries;
};

router.put("/", async (req, res) => {
  try {
    req.body.map((entry) => {
      entry.fields.map((field) => {
        if (field._id.slice(0, 3) === "new") {
          return field;
        }

        return {
          _id: mongoose.Types.ObjectId(field._id),
          sense: field.sense,
          bodyText: field.bodyText,
        };
      });
    });
  } catch (err) {
    console.error(err);
    res.status(400);
    res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
    res.set({ "Content-Type": "application/json" });
    res.send({
      err: "You sent invalid data. The PUT request could not be processed.",
    });
  }

  if (res.headersSent) return;

  const indexCleanedPUTEntries = indexCleanAllFields(req);

  await indexCleanedPUTEntries.forEach(async (PUTEntry) => {
    await mongooseSchemas.Entry.findOneAndUpdate(
      {
        _id: PUTEntry._id,
      },
      { $set: { fields: PUTEntry.fields } },
      { useFindAndModify: false },
      (err) => {
        if (err) {
          console.error(err);
          return dbErrorMessage(res);
        }
      }
    );
  });

  if (res.headersSent) return;
  const updatedEntries = await mongooseSchemas.Entry.find({}, {}, (err) => {
    if (err) {
      console.error(err);
      return dbErrorMessage(res);
    }
  })
    .sort({ date: "asc" })
    .lean();

  if (updatedEntries.length === 0) return dbErrorMessage(res);

  if (res.headersSent) return;
  res.status(201);
  res.header("Access-Control-Allow-Origin", "*"); //every address can retrieve data from the API
  res.set({ "Content-Type": "application/json" });
  res.send(JSON.stringify(updatedEntries));
});

module.exports = { router, connectDB };
