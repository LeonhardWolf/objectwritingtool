const fs = require("fs");
const mongoose = require("mongoose");

const readTxt = (txtPath, encoding) => {
  return fs.readFileSync(txtPath, encoding);
};

const splitLines = (txtString) => {
  return txtString.split("\n");
};

const filterEmptyElements = (txtArray) => {
  return txtArray.filter((string) => string !== "");
};

const extractLinesFromTxt = (txtPath, encoding) => {
  const txtString = readTxt(txtPath, encoding);
  const txtArray = splitLines(txtString);
  const filteredTxtArray = filterEmptyElements(txtArray);
  return filteredTxtArray;
};

const dailyObjectSchema = new mongoose.Schema({
  object: String,
});

const DailyObject = mongoose.model("DailyObject", dailyObjectSchema);

const capitalizeFirstLetter = (string) => {
  const capitalizedString = string.charAt(0).toUpperCase() + string.slice(1);
  return capitalizedString;
};

const saveDailyTxtObjects = async (txtPath, encoding) => {
  const filteredTxtArray = extractLinesFromTxt(txtPath, encoding);
  const capitalizedTxtArray = filteredTxtArray.map((string) =>
    capitalizeFirstLetter(string)
  );
  const newDocuments = capitalizedTxtArray.map(
    (stringElement) => new DailyObject({ object: stringElement })
  );
  await DailyObject.create(newDocuments);
};

const getRandomWordObject = async (math = Math) => {
  const documentsCount = await DailyObject.countDocuments({});
  const randomDocumentIndex = Math.floor(math.random() * documentsCount);
  const randomWordObject = await DailyObject.findOne(
    {},
    { _id: false, __v: false }
  ).skip(randomDocumentIndex);
  const randomWord = randomWordObject.object;
  return randomWord;
};

const senses = [
  "sight",
  "hearing",
  "smell",
  "taste",
  "touch",
  "organic",
  "kinesthetic",
];

const getNextSense = (lastSense) => {
  const lastIndex = senses.findIndex((sense) => sense === lastSense);
  if (lastIndex === -1) return console.error("next sense could not be found.");

  const sensesLength = senses.length;
  let nextIndex;

  if (lastIndex + 1 === sensesLength) {
    nextIndex = lastIndex + 1 - sensesLength;
  } else {
    nextIndex = lastIndex + 1;
  }

  const nextSense = senses[nextIndex];
  return nextSense;
};

module.exports = {
  saveDailyTxtObjects,
  DailyObject,
  getRandomWordObject,
  getNextSense,
};
