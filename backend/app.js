const express = require("express");
const entries = require("./routes/api/entries.js");
const app = express();
const cors = require("cors");

app.use(express.json());
app.use(cors());
app.use("/", express.static("./public"));
app.use("/api/entries", entries.router);

module.exports = app;
