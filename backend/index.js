const app = require("./app");
const { connectDB } = require("./routes/api/entries");
const { saveDailyTxtObjects } = require("./middleware/dailyObjects");

const PORT = process.env.OBJECTWRITINGTOOL_NODE_PORT || 5000;

(async () => {
  await connectDB();
})();

(async () => {
  await saveDailyTxtObjects("./assets/nounList.txt", "UTF-8");
})();

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});

module.exports = app;
