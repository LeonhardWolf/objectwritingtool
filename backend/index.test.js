const mongoose = require("mongoose");

const dailyObjects = require("./middleware/dailyObjects");

let connection;
let db;

beforeAll(async () => {
  db = await mongoose.connection;
  connection = await mongoose.connect(
    "mongodb://localhost:27017/testDatabase",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", () => console.log("Connected to db"));
});

afterAll(async () => {
  await db.dropDatabase();
  await db.close();
});

describe("Initially fill 'DailyObjects' collection with the .txt parsed word objects.", () => {
  jest.mock("./routes/api/entries", () => {
    return {
      connectDB: () => {},
    };
  });
  jest.mock("./app.js", () => {
    return {
      listen: (port, callback) => {},
    };
  });

  it("Collection 'DailyObjects' should be empty.", async () => {
    const saveDailyTxtObjectsSpy = jest
      .spyOn(dailyObjects, "saveDailyTxtObjects")
      .mockImplementationOnce((textPath, encoding) => {});

    const DailyObject = dailyObjects.DailyObject;
    const dailyObjectContent = await DailyObject.find({});
    expect(dailyObjectContent).toStrictEqual([]);
  });

  it("Should call 'saveDailyTxtObject()'.", () => {
    jest
      .spyOn(dailyObjects, "saveDailyTxtObjects")
      .mockImplementationOnce((textPath, encoding) => {});

    const index = require("./index");
    expect(dailyObjects.saveDailyTxtObjects).toHaveBeenCalledWith(
      "./assets/nounList.txt",
      "UTF-8"
    );
  });
});
