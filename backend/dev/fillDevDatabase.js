const mongoose = require("mongoose");

const mongooseSchemas = require("../routes/api/mongooseSchemas");
const dailyObjects = require("../middleware/dailyObjects");

let connection;
let db;

const connectDb = async () => {
  db = await mongoose.connection;

  if (!db._hasOpened) {
    connection = await mongoose.connect(
      "mongodb://localhost:27017/ObjectWritingTool_DB",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
    );
    db.on("error", console.error.bind(console, "connection error:"));
    db.once("open", () => console.log("Connected to db"));
  }
};

const saveEntries = async () => {
  const fields1 = [
    new mongooseSchemas.Field({
      sense: "seeing",
      bodyText: "I belong to bucket.",
    }),
    new mongooseSchemas.Field({
      sense: "hearing",
      bodyText: "I belong to bucket, too.",
    }),
  ];
  const entry1 = new mongooseSchemas.Entry({
    title: "Bucket",
    translation: "Eimer",
    date: new Date("2021-08-17T00:00:00.000Z"),
    fields: fields1,
  });
  await entry1.save();

  const fields2 = [
    new mongooseSchemas.Field({
      sense: "smell",
      bodyText: "I belong to table.",
    }),
    new mongooseSchemas.Field({
      sense: "taste",
      bodyText: "I belong to table, too.",
    }),
  ];
  const entry2 = new mongooseSchemas.Entry({
    title: "Table",
    translation: "Tisch",
    date: new Date("2021-08-18T00:00:00.000Z"),
    fields: fields2,
  });
  await entry2.save();

  const fields3 = [
    new mongooseSchemas.Field({
      sense: "touch",
      bodyText: "I belong to fence.",
    }),
    new mongooseSchemas.Field({
      sense: "organic",
      bodyText: "I belong to fence, too.",
    }),
  ];
  const entry3 = new mongooseSchemas.Entry({
    title: "Fence",
    translation: "Zaun",
    date: new Date("2021-08-19T00:00:00.000Z"),
    fields: fields3,
  });
  await entry3.save();
};

const refillDevDatabaseEntries = async () => {
  await connectDb();
  await mongooseSchemas.Entry.deleteMany({});
  await saveEntries();
};

const refillDevDatabaseDailyObjects = async (txtPath, encoding) => {
  await connectDb();
  await dailyObjects.DailyObject.deleteMany({});
  await dailyObjects.saveDailyTxtObjects(txtPath, encoding);
};

module.exports = { refillDevDatabaseEntries, refillDevDatabaseDailyObjects };
